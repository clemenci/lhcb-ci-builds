import common.Common

def check_preconditions = job('check-preconditions') {
  concurrentBuild true
  parameters {
    string {
      name 'platform'
      description 'platform to check the preconditions for'
      trim true
    }
  }
  properties {
    rateLimitBuilds { throttle {
      count 12
      durationName "hour"
      userBoost true
    }}
  }
  wrappers {
    buildName('${task}/${platform} (#${BUILD_NUMBER})')
  }
  steps {
    shell(readFileFromWorkspace('jenkins/scripts/check-preconditions.sh'))
    triggerBuilder {
      configs {
        blockableBuildTriggerConfig {
          projects 'scheduler'
          configs { gitRevisionBuildParameters { combineQueuedCommits false } }
          configFactories {
            fileBuildParameterFactory {
              filePattern 'data/scheduler_params.txt'
              encoding 'UTF-8'
              noFilesFoundAction 'SKIP'
            }
          }
          // not wanted, but we cannot configure otherwise (dropped later in configuration post-processing)
          block {
            buildStepFailureThreshold ''
            unstableThreshold ''
            failureThreshold ''
          }
        }
        blockableBuildTriggerConfig {
          projects 'check-preconditions'
          configs { gitRevisionBuildParameters { combineQueuedCommits false } }
          configFactories {
            fileBuildParameterFactory {
              filePattern 'data/preconditions-*.txt'
              encoding 'UTF-8'
              noFilesFoundAction 'SKIP'
            }
          }
          // not wanted, but we cannot configure otherwise (dropped later in configuration post-processing)
          block {
            buildStepFailureThreshold ''
            unstableThreshold ''
            failureThreshold ''
          }
        }
      }
    }
  }
  configure { node -> Common.nonBlockingTriggers(node) }
}

Common.setScm(check_preconditions)
Common.addParameters(check_preconditions)
Common.rotateLogs(check_preconditions)
Common.retry(check_preconditions)
