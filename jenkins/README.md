This directory contains the file needed to bootstrap the configuration of Jenkins.

**Note**: this is very much WIP

To create a Docker/Podman container image to run LHCb CI Builds need to
configure a CERN SSO application at <https://application-portal.web.cern.ch>.

It's important to assign the role `admin` to a group and to set as allowed
redirect URL something like `${JENKINS_ROOT_URL}/securityRealm/finishLogin` (see
[OpenId Connect Authentication plugin](https://plugins.jenkins.io/oic-auth/)).
We also need the role `swarm` for a less privileged group to allow automatic
configuration of build nodes.

A server certificate must be created and called
`jenkins.p12` (see, for example, <https://www.baeldung.com/spring-boot-https-self-signed-certificate#generating-keystore>).

Then the docker image can be created with
```bash
podman build -t jenkins --build-arg CERT_PASSWORD=${CERTIFICATE_PASSWORD} .
```
and it can be run with something like (see <https://github.com/jenkinsci/docker>)
```bash
podman run --name jenkins -p 8080:8080 -p 8083:8083 -p 50000:50000 \
    --restart-on-failure	\
    --env OAUTH_ID=${OAUTH_ID} --env OAUTH_SECRET=${OAUTH_SECRET} \
    -v jenkins_home:/var/jenkins_home jenkins
```

Then connect to <https://localhost:8083>.

For production, on lhcb-jenkins.cern.ch, we create the container with (as root)
```bash
podman create --name jenkins -p 80:8080 -p 443:8083 -p 50000:50000 \
    --restart-on-failure	\
    --env OAUTH_ID=${OAUTH_ID} --env OAUTH_SECRET=${OAUTH_SECRET} \
    -v jenkins_home:/var/jenkins_home jenkins
```
and start it with `podman start jenkins`.

The image is built preloading a number of plugins, including
- [OpenId Connect Authentication](https://plugins.jenkins.io/oic-auth/): authenticate with CERN SSO
- [Configuration as Code](https://plugins.jenkins.io/configuration-as-code/): be able to use the special file `jenkins.yaml` to configure the Jenkins instance
- [Job DSL](https://plugins.jenkins.io/job-dsl/): create and update the CI jobs from configuration files
- [Swarm](https://plugins.jenkins.io/swarm/): allow nodes to automatically join the build cluster
- [Node and Label parameter](https://plugins.jenkins.io/nodelabelparameter/): use a parameter to decide where a job can run
- [Naginator](https://plugins.jenkins.io/naginator/): automatically restart failed jobs
- [Rebuilder](https://plugins.jenkins.io/rebuild/): add a button to rerun the job, optionally changing parameters

Since the Swarm plugin requires to connect via an API token (to bypass OpenID)
and API tokens cannot be created progammatically we have to create one from the
web interface for a user that has `swarm` role.

Some job requires credentials and they can be obtained from environment or filesystem.
If the scripts needs the credentials from the environment we can use Jenkins
to pass them, like for the  Gitlab Personal Access Token used by checkout to
publish messages to Gitlab. The Checkout job is already correctly configured, to get
the token from the Jenkins credentials provider, but the correct value should be
created in Gitlab and saved via the Jenkins web interface.

Secrets to be set after creation of Jenkins:
- `lhcbsoft-gitlab-token`
- `lbtasks-token`
- `gitlab-ssh-key`
- `start-slots-token` (for clients to be able to trigger the job from a URL
  like `JENKINS_URL/buildByToken/buildWithParameters?job=start-slots&token=TOKEN&slots=slot1+slot2`)

The built-in node does not have executors, so to be able to do anything
(including running the seed job to set up the actual jobs driving the builds) we
have to connect a node with something like
```bash
export JENKINS_URL=https://localhost:8083
curl -k $JENKINS_URL/userContent/start_worker.sh | env SWARM_USER=someuser SWARM_TOKEN=generated_token sh
```
or, for the production isntance,
```bash
curl https://lhcb-jenkins.cern.ch/userContent/start_worker.sh | env SWARM_USER=someuser SWARM_TOKEN=generated_token sh
```
