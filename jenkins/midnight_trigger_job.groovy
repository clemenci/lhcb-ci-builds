import common.Common

def midnight_trigger = job('midnight-trigger') {
  description('''job automatically started every night at midnight to trigger all enabled slots''')
  triggers {
    cron('TZ=Europe/Zurich\n0 1 * * *')
  }
  label("schedule")
  steps {
    triggerBuilder {
      configs {
        blockableBuildTriggerConfig {
          projects 'start-slots'
          configs {
            predefinedBuildParameters {
              textParamValueOnNewLine(false)
              properties("slots=<all>")
            }
          }
          // not wanted, but we cannot configure otherwise (dropped later in configuration post-processing)
          block {
            buildStepFailureThreshold ''
            unstableThreshold ''
            failureThreshold ''
          }
        }
      }
    }
  }
  configure { node -> Common.nonBlockingTriggers(node) }
}

Common.rotateLogs(midnight_trigger)
Common.retry(midnight_trigger)
