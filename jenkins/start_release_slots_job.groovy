import common.Common

def start_slots = job('start-release-slots') {
  label("schedule")
  authenticationToken(START_SLOTS_TOKEN)
  triggers {
    urlTrigger {
      cron('H/5 * * * *')
      url('https://gitlab.cern.ch/api/v4/projects/lhcb-core%2Flhcbstacks/merge_requests?state=opened&wip=no') {
        inspection('change')
      }
    }
  }
  steps {
    shell(readFileFromWorkspace('jenkins/scripts/start_release_slots.sh'))
    triggerBuilder {
      configs {
        blockableBuildTriggerConfig {
          projects 'scheduler'
          configs { gitRevisionBuildParameters { combineQueuedCommits false } }
          configFactories {
            fileBuildParameterFactory {
              filePattern 'data/slot-*.txt'
              encoding 'UTF-8'
              noFilesFoundAction 'SKIP'
            }
          }
          // not wanted, but we cannot configure otherwise (dropped later in configuration post-processing)
          block {
            buildStepFailureThreshold ''
            unstableThreshold ''
            failureThreshold ''
          }
        }
      }
    }
  }
  wrappers {
    credentialsBinding {
      string {
      	variable("GITLAB_TOKEN")
        credentialsId("lhcbsoft-gitlab-token")
      }
    }
  }
  configure { node -> Common.nonBlockingTriggers(node) }
}

Common.setScm(start_slots)
Common.rotateLogs(start_slots)
Common.retry(start_slots)
