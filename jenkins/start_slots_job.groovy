import common.Common

def start_slots = job('start-slots') {
  parameters {
    choiceParam(
      'flavour',
      ['nightly', 'testing'],
      '''which database partition to use for the slots'''
    )
    string {
      name 'slots'
      description '''which slots to trigger (use "<all>" to run all enabled slots)'''
      trim true
    }
    string {
      name 'config_repo'
      description '''Git repository and/or version to use to get slots configurations from.

      Leave empty to use https://gitlab.cern.ch/lhcb-core/LHCbNightlyConf.git or pass #<commit-ish> for a specific version'''
    }
  }
  concurrentBuild true
  label("schedule")
  authenticationToken(START_SLOTS_TOKEN)
  steps {
    shell(readFileFromWorkspace('jenkins/scripts/start_slots.sh'))
    triggerBuilder {
      configs {
        blockableBuildTriggerConfig {
          projects 'scheduler'
          configs { gitRevisionBuildParameters { combineQueuedCommits false } }
          configFactories {
            fileBuildParameterFactory {
              filePattern 'data/slot-*.txt'
              encoding 'UTF-8'
              noFilesFoundAction 'SKIP'
            }
          }
          // not wanted, but we cannot configure otherwise (dropped later in configuration post-processing)
          block {
            buildStepFailureThreshold ''
            unstableThreshold ''
            failureThreshold ''
          }
        }
      }
    }
  }
  configure { node -> Common.nonBlockingTriggers(node) }
}

Common.setScm(start_slots)
Common.rotateLogs(start_slots)
Common.retry(start_slots)
