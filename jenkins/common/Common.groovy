package common

class Common {
  static String repoUrl() {
    return 'https://gitlab.cern.ch/lhcb-core/nightly-builds/lhcb-ci-builds.git'
  }
  static void setScm(def job) {
    job.with {
      scm {
        git(Common.repoUrl(), "refs/heads/main") { node ->
          // prevent Job DSL implicit perBuildTag
          def ext = node / 'extensions'
          ext.remove( ext.children()[0] )
        }
      }
    }
  }
  static void addParameters(def job) {
    job.with {
      parameters {
        string {
          name 'task'
          description '''what to do in the form "<flavour>/<name>/<build_id>"'''
          trim true
        }
      }
    }
  }
  static Node nonBlockingTriggers(def node) {
    (
      ( node / 'builders' ).children().stream()
      .filter { step -> step.name() == "hudson.plugins.parameterizedtrigger.TriggerBuilder" }
      .each { trigger ->
        (trigger / 'configs').children().stream()
        .filter { cfg -> cfg.name() == "hudson.plugins.parameterizedtrigger.BlockableBuildTriggerConfig" }
        .each { cfg ->
          cfg.remove( cfg / 'block' )
        }
      }
    )
    return node
  }
  static void rotateLogs(def job) {
    job.with {
      logRotator{ daysToKeep(15) }
    }
  }
  static void retry(def job) {
    job.with {
      publishers {
        // FIXME this is the same retry policy of the old system, should we improve?
        retryBuild {
          rerunIfUnstable(false)
          retryLimit(2)
          fixedDelay(300)
        }
      }
    }
  }
}
