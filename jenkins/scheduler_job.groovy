import common.Common

def scheduler = job('scheduler') {
  authenticationToken(SCHEDULER_TOKEN)
  label('schedule')
  wrappers {
    buildName('${task} (#${BUILD_NUMBER})')
  }
  steps {
    shell(readFileFromWorkspace('jenkins/scripts/scheduler.sh'))
    triggerBuilder {
      configs {
        blockableBuildTriggerConfig {
          projects 'checkout'
          configs { gitRevisionBuildParameters { combineQueuedCommits false } }
          configFactories {
            fileBuildParameterFactory {
              filePattern 'data/checkout.txt'
              encoding 'UTF-8'
              noFilesFoundAction 'SKIP'
            }
          }
          // not wanted, but we cannot configure otherwise (dropped later in configuration post-processing)
          block {
            buildStepFailureThreshold ''
            unstableThreshold ''
            failureThreshold ''
          }
        }
        blockableBuildTriggerConfig {
          projects 'check-preconditions'
          configs { gitRevisionBuildParameters { combineQueuedCommits false } }
          configFactories {
            fileBuildParameterFactory {
              filePattern 'data/preconditions-*.txt'
              encoding 'UTF-8'
              noFilesFoundAction 'SKIP'
            }
          }
          // not wanted, but we cannot configure otherwise (dropped later in configuration post-processing)
          block {
            buildStepFailureThreshold ''
            unstableThreshold ''
            failureThreshold ''
          }
        }
        blockableBuildTriggerConfig {
          projects 'build'
          configs { gitRevisionBuildParameters { combineQueuedCommits false } }
          configFactories {
            fileBuildParameterFactory {
              filePattern 'data/build-*.txt'
              encoding 'UTF-8'
              noFilesFoundAction 'SKIP'
            }
          }
          // not wanted, but we cannot configure otherwise (dropped later in configuration post-processing)
          block {
            buildStepFailureThreshold ''
            unstableThreshold ''
            failureThreshold ''
          }
        }
      }
    }
  }
  configure { node -> Common.nonBlockingTriggers(node) }
}

Common.setScm(scheduler)
Common.addParameters(scheduler)
Common.rotateLogs(scheduler)
Common.retry(scheduler)
