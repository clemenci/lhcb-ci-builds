###############################################################################
# (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# This script prepares the environment to run the nightly builds tasks

# Validate the execution environment
if [ -z "$WORKSPACE" ] ; then
  echo 'error: the environment does not look like a Jenkins job ($WORKSPACE not defined)'
  exit 1
fi

if [ ! -r "$WORKSPACE/jenkins/scripts/prepare_env.sh" ] ; then
  echo 'error: $WORKSPACE should point to the directory containing this file'
  exit 1
fi

# use a temporary directory within the workspace
# (it's needed to make it visible to the apptainer container)
export TMPDIR=$WORKSPACE/tmp
# FIXME we tend to pollute the TMPDIR, so we clean it up
rm -rf "$TMPDIR"
# make sure it exists
mkdir -p "$TMPDIR"

case $(uname -m) in
  x86_64 ) conda_arch=linux-64 ;;
  aarch64 ) conda_arch=linux-aarch64 ;;
  * ) echo "unsupported arch $(uname -m)" ; exit 1 ;;
esac

if [ $conda_arch = "linux-64" ] ; then
  # add CMT to the path
  export PATH=/cvmfs/lhcb.cern.ch/lib/bin/Linux-x86_64:$PATH
fi

conda_exe=/cvmfs/lhcbdev.cern.ch/conda/miniconda/${conda_arch}/prod/condabin/conda

# Check if we have an externally provided conda environment hash
if [ -z "$env_hash" ] ; then
  # otherwise we get the default from LHCbNightlyConf
  curl -L --output list-environments.zip "https://gitlab.cern.ch/lhcb-core/LHCbNightlyConf/-/jobs/artifacts/master/download?job=list-environments"
  export env_hash=$(unzip -p list-environments.zip env-hashes.yaml | awk '/^[^ ]+:/{env=$1} /^  '${conda_arch}':/{if (env == "legacy:") {print $2; exit}}')
  rm -f list-environments.zip
  # make sure we got the hash
  test -n "$env_hash"
fi

# wait up to one hour for the CVMFS directory to appear
echo "$(date -Is) - waiting for /cvmfs/lhcbdev.cern.ch/nightly-environments/$env_hash/bin"
for cnt in $(seq 0 60) ; do
  if [ -r /cvmfs/lhcbdev.cern.ch/nightly-environments/$env_hash/bin ] ; then
    echo "$(date -Is) - found"
    break
  fi
  echo "."
  sleep 60
done
test -r /cvmfs/lhcbdev.cern.ch/nightly-environments/$env_hash/bin
echo "$(date -Is) - waited for $cnt minutes"

# make sure we are not already in a conda environment
# (it may cause invalid environments)
while [ -n "$CONDA_PREFIX" ] ; do
  eval "$($conda_exe shell.posix deactivate)"
done
# activate the requested environment
eval "$($conda_exe shell.posix activate /cvmfs/lhcbdev.cern.ch/nightly-environments/$env_hash)"
PS1="(cvmfs env $(echo $env_hash | cut -b1-10)) "

# make sure we use the right cacert file in all cases
export SSL_CERT_FILE=$(python -m certifi)

# FIXME uv should be installed in the conda environment
#       see https://gitlab.cern.ch/lhcb-core/LHCbNightlyConf/-/merge_requests/1286
if ! which uv >& /dev/null ; then
  export XDG_BIN_HOME=$WORKSPACE/.local/bin
  mkdir -p $XDG_BIN_HOME
  ln -sf /cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/stable/$conda_arch/bin/uv $XDG_BIN_HOME/uv
  export PATH=$XDG_BIN_HOME:$PATH
fi

# FIXME this is a bit of a hack, but some nodes cannot reach PyPI and
#       we cannot use the local mirror for all jobs (it fails under heavy load)
if ! ping -q -c 1 -w 1 pypi.org >& /dev/null ; then
  echo "PyPI is not reachable, use local mirror"
  use_mirror="--default-index https://lhcb-repository.web.cern.ch/repository/pypi/simple"
else
  use_mirror=""
fi

# install the requirements
uv venv --system-site-packages --python $(which python) $WORKSPACE/.local_venv
. $WORKSPACE/.local_venv/bin/activate
uv pip install $use_mirror -r requirements.lock

# FIXME this is needed to be able to talk to lhcbpr.cern.ch
# make sure we can patch the cacert file
uv pip install $use_mirror --force-reinstall certifi
# use the just installed cacert file
export SSL_CERT_FILE=$(python -m certifi)
# Patch the cacert file with extra CA certificates needed to trust lhcbpr.cern.ch
cat $WORKSPACE/cern-sectigo-ca-bundle.crt >> $SSL_CERT_FILE

# prepare OS specific overrides to the environment
rm -rf $WORKSPACE/.overrides
for os in slc6 centos7 el9 ; do
  mkdir -p $WORKSPACE/.overrides/$os/bin
done

base=/cvmfs/lhcbdev.cern.ch/nightly-environments/d793e9f5b9f35ede449593454325378b56dfde173d823ea6e978b81e5bcba3af
for f in ccache ninja python python3 git ; do
  ln -sf $base/bin/$f $WORKSPACE/.overrides/slc6/bin
done
# we need an old version of cmake to build on slc6
for f in cmake ctest ; do
  ln -sf /cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.17.3/Linux-x86_64/bin/$f $WORKSPACE/.overrides/slc6/bin
done

ln -s ../../../jenkins/scripts/lbn-wrapcmd $WORKSPACE/.overrides/slc6/bin

# we use SLC6 containers to build for SLC5
ln -sfT slc6 $WORKSPACE/.overrides/slc5

unset os base

# FIXME some jobs may use the special variable LBN_BUILD_JOBS
#       in particular with LHCbPR
export LBN_BUILD_JOBS="${CORES_PER_EXECUTOR}"
