#/bin/bash -xe
set +x
echo "preparing environment ..."
. jenkins/env
set -x

# prepare
rm -rf data
mkdir data
cd data

set +x
echo "Cluster metrics: https://monit-grafana.cern.ch/d/000000116/host-metrics?orgId=1&var-hostgroup=volhcb%2Flhcbbuild%2Fjenkins_worker"
echo "Host metrics:    https://monit-grafana.cern.ch/d/RwtmMDXmz/host-metrics-simple?var-hostname=$(hostname -s).cern.ch&orgId=1"
set -x

# Record infos for debugging
date +"%c%n%Y-%m-%dT%H:%M:%S%:z%n%s" | tee timestamp.txt
hostname | tee hostname.txt
printenv BUILD_URL | tee build_url.txt
printenv | zstd > env.txt.zst

if [ -z "${slots}" ] ; then
    echo "slots parameter is not set, aborting (use '<all>' to start all slots)"
    exit 1
fi

# We use the "<all>" special value to instead of the empty string to mean
# all slots instead of an empty string (expected by lb-ci config get-slots)
# to avoid accidentally file a massive build.
if [ "${slots}" == "<all>" ] ; then
    slots=""
fi

lb-ci config get-slots ${config_repo:+--config-repo $config_repo} --output slot.json ${slots}
lb-ci config resolve-mrs ${CORES_PER_EXECUTOR:+-j ${CORES_PER_EXECUTOR}} --output slots_with_mrs.json slot.json
if [ -z "${slots}" ] ; then
    # no explicit list of slots, so we drop duplicates
    lb-ci config remove-duplicates ${flavour:+--flavour $flavour} --output slots_to_build.json slots_with_mrs.json
else
    # slots explicitly requested, we use what we have
    cp slots_with_mrs.json slots_to_build.json
fi
lb-ci config publish ${flavour:+--flavour $flavour} slots_to_build.json | tee slots.txt
i=0
for slot in $(cat slots.txt) ; do
    echo "task=${slot}" > slot-${i}.txt
    i=$(( $i + 1 ))
done

# partial clean up (keep only entries smaller than 500kb)
du -d 1 -a -b -t 500000 | grep -v '\.$' | xargs -t -r rm -rf

# back up data dir (for debugging)
cd ..
cp -a data data.$(date -Is)
# keep up to 40 backups
( ls | grep ^data\. | tail -40 ; ls | grep ^data\. ) | sort | uniq --uniq | xargs --no-run-if-empty rm -rfv
