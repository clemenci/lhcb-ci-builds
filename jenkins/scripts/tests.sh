#/bin/bash -xe
# note: test script is run in the same job as build, but we have to set up the environment agains
# FIXME deduplicate
set +x
echo "preparing environment ..."
. jenkins/env
set -x

ARTIFACTS_REPO='{
    "uri": "root://eosproject.cern.ch//eos/project/l/lhcbwebsites/www/lhcb-nightlies-artifacts",
    "user": "lhcbsoft@CERN.CH",
    "keytab": "'"$HOME/private/lhcbsoft.keytab"'",
    "base_url": "https://lhcb-nightlies-artifacts.web.cern.ch/"
}'

KRB5_AUTH='{
    "principal": "lhcbsoft@CERN.CH",
    "keytab": "'"$HOME/private/lhcbsoft.keytab"'"
}'

cd data
# hide the lhcb-ci-builds pyproject.toml from pytest
# (already done in the build, but better be sure)
touch pytest.ini
# FIXME Allen runs pytest from $TMPDIR, so we need to this too
touch $TMPDIR/pytest.ini

# Record infos for debugging
date +"%c%n%Y-%m-%dT%H:%M:%S%:z%n%s" | tee timestamp-tests.txt
echo https://lhcb-nightlies.web.cern.ch/${task}/${project}/tests > tests_log_url.txt
printenv | zstd > env-tests.txt.zst

lb-ci test ${CORES_PER_EXECUTOR:+-j ${CORES_PER_EXECUTOR}} -a "${ARTIFACTS_REPO}" --with-krb5 "${KRB5_AUTH}" ${task} ${project}
lb-ci publish-new-refs -a "${ARTIFACTS_REPO}" ${task} ${project}

# note: the setup was done by the build script, we just have to clean up

# partial clean up (keep only entries smaller than 500kb)
du -d 1 -a -b -t 500000 | grep -v '\.$' | xargs -t -r rm -rf

# back up data dir (for debugging)
cd ..
cp -a data data.$(date -Is)
# keep up to 40 backups
( ls | grep ^data\. | tail -40 ; ls | grep ^data\. ) | sort | uniq --uniq | xargs --no-run-if-empty rm -rfv
