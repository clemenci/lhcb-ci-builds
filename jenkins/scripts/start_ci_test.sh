#/bin/bash -xe
set +x
echo "preparing environment ..."
. jenkins/env
set -x

# prepare
rm -rf data
mkdir data
cd data

set +x
echo "Cluster metrics: https://monit-grafana.cern.ch/d/000000116/host-metrics?orgId=1&var-hostgroup=volhcb%2Flhcbbuild%2Fjenkins_worker"
echo "Host metrics:    https://monit-grafana.cern.ch/d/RwtmMDXmz/host-metrics-simple?var-hostname=$(hostname -s).cern.ch&orgId=1"
set -x

# Record infos for debugging
date +"%c%n%Y-%m-%dT%H:%M:%S%:z%n%s" | tee timestamp.txt
hostname | tee hostname.txt
printenv BUILD_URL | tee build_url.txt
printenv | zstd > env.txt.zst

if [ -z "${webhook_data}" ] ; then
    echo "webhook_data parameter is not set, aborting"
    exit 1
fi

lbn-gitlab-mr \
    --debug \
    --hook-var "webhook_data" \
    --output "gitlab-slots-build.txt" \
    --feedback
if [ -e gitlab-slots-build.txt ] ; then
    lb-ci config ci-test ${config_repo:+--config-repo $config_repo} --output slot.json --from-file gitlab-slots-build.txt
    lb-ci config resolve-mrs ${CORES_PER_EXECUTOR:+-j ${CORES_PER_EXECUTOR}} --output slots_with_mrs.json slot.json
    lb-ci config remove-duplicates ${flavour:+--flavour $flavour} --output slots_to_build.json slots_with_mrs.json
    lb-ci config publish ${flavour:+--flavour $flavour} slots_to_build.json | tee slots.txt

    i=0
    for slot in $(cat slots.txt) ; do
        echo "task=${slot}" > slot-${i}.txt
        i=$(( $i + 1 ))
    done
else
    echo "gitlab-slots-build.txt not produced, nothing to do"
    exit 1
fi

# partial clean up (keep only entries smaller than 500kb)
du -d 1 -a -b -t 500000 | grep -v '\.$' | xargs -t -r rm -rf

# back up data dir (for debugging)
cd ..
cp -a data data.$(date -Is)
# keep up to 40 backups
( ls | grep ^data\. | tail -40 ; ls | grep ^data\. ) | sort | uniq --uniq | xargs --no-run-if-empty rm -rfv
