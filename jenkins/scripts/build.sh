#/bin/bash -xe
# FIXME deduplicate
set +x
echo "preparing environment ..."
. jenkins/env
set -x

ARTIFACTS_REPO='{
    "uri": "root://eosproject.cern.ch//eos/project/l/lhcbwebsites/www/lhcb-nightlies-artifacts",
    "user": "lhcbsoft@CERN.CH",
    "keytab": "'"$HOME/private/lhcbsoft.keytab"'",
    "base_url": "https://lhcb-nightlies-artifacts.web.cern.ch/"
}'

KRB5_AUTH='{
    "principal": "lhcbsoft@CERN.CH",
    "keytab": "'"$HOME/private/lhcbsoft.keytab"'"
}'

# prepare
rm -rf data
mkdir data
cd data
# hide the lhcb-ci-builds pyproject.toml from pytest
touch pytest.ini
# FIXME Allen runs pytest from $TMPDIR, so we need to this too
touch $TMPDIR/pytest.ini

set +x
echo "Cluster metrics: https://monit-grafana.cern.ch/d/000000116/host-metrics?orgId=1&var-hostgroup=volhcb%2Flhcbbuild%2Fjenkins_worker"
echo "Host metrics:    https://monit-grafana.cern.ch/d/RwtmMDXmz/host-metrics-simple?var-hostname=$(hostname -s).cern.ch&orgId=1"
set -x

# Record infos for debugging
date +"%c%n%Y-%m-%dT%H:%M:%S%:z%n%s" | tee timestamp-build.txt
echo https://lhcb-nightlies.web.cern.ch/${task} > slot_url.txt
echo https://lhcb-nightlies.web.cern.ch/${task}/${project}/build > build_log_url.txt
hostname | tee hostname.txt
printenv BUILD_URL | tee build_url.txt
printenv | zstd > env-build.txt.zst

# FIXME LHCbIntegrationTests detects the nightly builds environment from the variables slot and slot_build_id
export slot=$(echo ${task} | cut -d/ -f2)
export slot_build_id=$(echo ${task} | cut -d/ -f3)

lb-ci build ${CORES_PER_EXECUTOR:+-j ${CORES_PER_EXECUTOR}} -a "${ARTIFACTS_REPO}" --with-krb5 "${KRB5_AUTH}" ${task} ${project}
lb-ci deploy build -a "${ARTIFACTS_REPO}" ${task} ${project}

lb-ci schedule-pr-tests ${task} ${project}

# FIXME it seems that rsync does not work when invoked from systemctl service
(
    mkdir -p $TMPDIR/bin
    ln -s $(which cp) $TMPDIR/bin/rsync
    export PATH=$TMPDIR/bin:$PATH
    lb-ci rpm make-bin -a "${ARTIFACTS_REPO}" ${task} ${project}
    rm -rf $TMPDIR/bin
)

lb-ci rpm make-index -a "${ARTIFACTS_REPO}" ${task} ${project}

echo "task=${task}" > scheduler_params.txt

# note: the clean up happens in the test script
