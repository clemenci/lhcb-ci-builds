#/bin/bash -xe
set +x
echo "preparing environment ..."
. jenkins/env
set -x

# prepare
rm -rf data
mkdir data
cd data

set +x
echo "Cluster metrics: https://monit-grafana.cern.ch/d/000000116/host-metrics?orgId=1&var-hostgroup=volhcb%2Flhcbbuild%2Fjenkins_worker"
echo "Host metrics:    https://monit-grafana.cern.ch/d/RwtmMDXmz/host-metrics-simple?var-hostname=$(hostname -s).cern.ch&orgId=1"
set -x

# Release builds always go to flavour release
export flavour=release

# Record infos for debugging
date +"%c%n%Y-%m-%dT%H:%M:%S%:z%n%s" | tee timestamp.txt
hostname | tee hostname.txt
printenv BUILD_URL | tee build_url.txt
printenv | zstd > env.txt.zst

lb-ci config get-release-slots --flavour ${flavour} --output slot.json
lb-ci config publish --flavour ${flavour} slot.json | tee slots.txt

i=0
for slot in $(cat slots.txt) ; do
    echo "task=${slot}" > slot-${i}.txt
    i=$(( $i + 1 ))
done

# partial clean up (keep only entries smaller than 500kb)
du -d 1 -a -b -t 500000 | grep -v '\.$' | xargs -t -r rm -rf

# back up data dir (for debugging)
cd ..
cp -a data data.$(date -Is)
# keep up to 40 backups
( ls | grep ^data\. | tail -40 ; ls | grep ^data\. ) | sort | uniq --uniq | xargs --no-run-if-empty rm -rfv
