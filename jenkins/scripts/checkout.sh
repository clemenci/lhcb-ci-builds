#/bin/bash -xe
# FIXME deduplicate
set +x
echo "preparing environment ..."
. jenkins/env
set -x

ARTIFACTS_REPO='{
    "uri": "root://eosproject.cern.ch//eos/project/l/lhcbwebsites/www/lhcb-nightlies-artifacts",
    "user": "lhcbsoft@CERN.CH",
    "keytab": "'"$HOME/private/lhcbsoft.keytab"'",
    "base_url": "https://lhcb-nightlies-artifacts.web.cern.ch/"
}'

# prepare
rm -rf data
mkdir data
cd data

set +x
echo "Cluster metrics: https://monit-grafana.cern.ch/d/000000116/host-metrics?orgId=1&var-hostgroup=volhcb%2Flhcbbuild%2Fjenkins_worker"
echo "Host metrics:    https://monit-grafana.cern.ch/d/RwtmMDXmz/host-metrics-simple?var-hostname=$(hostname -s).cern.ch&orgId=1"
set -x

# Record infos for debugging
date +"%c%n%Y-%m-%dT%H:%M:%S%:z%n%s" | tee timestamp.txt
echo https://lhcb-nightlies.web.cern.ch/${task} > slot_url.txt
hostname | tee hostname.txt
printenv BUILD_URL | tee build_url.txt
printenv | zstd > env.txt.zst

lb-ci checkout ${CORES_PER_EXECUTOR:+-j ${CORES_PER_EXECUTOR}} -a "${ARTIFACTS_REPO}" ${task}
lb-ci config update-dependencies ${task}
lb-ci deploy structure -a "${ARTIFACTS_REPO}" ${task}
lb-ci rpm make-src ${CORES_PER_EXECUTOR:+-j ${CORES_PER_EXECUTOR}} -a "${ARTIFACTS_REPO}" ${task}

# partial clean up (keep only entries smaller than 500kb)
du -d 1 -a -b -t 500000 | grep -v '\.$' | xargs -t -r rm -rf

echo "task=${task}" > scheduler_params.txt

# back up data dir (for debugging)
cd ..
cp -a data data.$(date -Is)
# keep up to 40 backups
( ls | grep ^data\. | tail -40 ; ls | grep ^data\. ) | sort | uniq --uniq | xargs --no-run-if-empty rm -rfv
