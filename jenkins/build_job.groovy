import common.Common

def build = job('build') {
  concurrentBuild true
  parameters {
    string {
      name 'project'
      description 'project to build (<name>/<platform>)'
      trim true
    }
    labelParam("node")
  }
  wrappers {
    buildName('${task}/${project} (#${BUILD_NUMBER})')
    credentialsBinding {
      string {
      	variable("GITLAB_TOKEN")
        credentialsId("lhcbsoft-gitlab-token")
      }
      string {
      	variable("LBTASKS_TOKEN")
        credentialsId("lbtasks-token")
      }
    }
  }
  steps {
    shell(readFileFromWorkspace('jenkins/scripts/build.sh'))
    triggerBuilder {
      configs {
        blockableBuildTriggerConfig {
          projects 'scheduler'
          configs { gitRevisionBuildParameters { combineQueuedCommits false } }
          // FIXME trigger of the scheduler should be unconditional
          configFactories {
            fileBuildParameterFactory {
              filePattern 'data/scheduler_params.txt'
              encoding 'UTF-8'
              noFilesFoundAction 'SKIP'
            }
          }
          // not wanted, but we cannot configure otherwise (dropped later in configuration post-processing)
          block {
            buildStepFailureThreshold ''
            unstableThreshold ''
            failureThreshold ''
          }
        }
        blockableBuildTriggerConfig {
          projects 'lhcb-pr-test'
          configs { gitRevisionBuildParameters { combineQueuedCommits false } }
          configFactories {
            fileBuildParameterFactory {
              filePattern 'data/lhcb-pr-test-*.txt'
              encoding 'UTF-8'
              noFilesFoundAction 'SKIP'
            }
          }
          // not wanted, but we cannot configure otherwise (dropped later in configuration post-processing)
          block {
            buildStepFailureThreshold ''
            unstableThreshold ''
            failureThreshold ''
          }
        }
      }
    }
    shell(readFileFromWorkspace('jenkins/scripts/tests.sh'))
  }
  configure { node -> Common.nonBlockingTriggers(node) }
}

Common.setScm(build)
Common.addParameters(build)
Common.rotateLogs(build)
Common.retry(build)
