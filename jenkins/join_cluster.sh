#!/bin/bash -e

# FIXME we should switch to https as soon as we have a certificate
JENKINS_URL=${JENKINS_URL:-https://lhcb-jenkins.cern.ch}

# Get the latest version of the client code
rm -f swarm-client.jar
curl -O -k ${JENKINS_URL}/swarm/swarm-client.jar

# Compute the slave configuration
if [ -z "${CUSTOM_LABELS}" ] ; then
  # we must have CVMFS, we can use LbPlatformUtils from there
  case $(uname -m) in
    x86_64) python_exe=/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/stable/linux-64/bin/python3 ;;
    aarch64) python_exe=/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/stable/linux-aarch64/bin/python3 ;;
    *) python_exe=python3 ;;
  esac
  archs=$($python_exe -c '
from LbPlatformUtils.architectures import ARCH_DEFS, get_supported_archs
from LbPlatformUtils.inspect import architecture
print(" ".join(get_supported_archs(ARCH_DEFS[architecture()])))
')
  labels="schedule build $archs ${EXTRA_LABELS}"
  # FIXME we can run checkout tasks only on x86_64 because they depend on CMT
  #       (data package checkout runs CMT)
  if [ $(uname -m) = "x86_64" ] ; then
    labels="$labels checkout"
  fi
else
  labels="${CUSTOM_LABELS}"
fi

# Compute the *ideal* number of executors and cores per executor
# - we allow one process per 2GB of RAM or per processing unit (whichever is smaller)
# - we allow one executor per 10GB of disk space
max_processes=$(awk '/^MemTotal:/{printf("%.0f\n", $2 / 2e6)}' /proc/meminfo)
max_processes=$(( max_processes < $(nproc) ? max_processes : $(nproc) ))
max_executors=$(df . -k | awk '!/Filesystem/{printf("%.0f\n", $2 / 10e6)}')
if [ -z "${CUSTOM_CORES_PER_EXECUTOR}" ] ; then
  cores_per_executor=$(( max_processes < 8 ? max_processes : 8 ))
else
  cores_per_executor=${CUSTOM_CORES_PER_EXECUTOR}
fi

if [ -z "${CUSTOM_EXECUTORS}" ] ; then
  executors=$(( max_processes / cores_per_executor ))
  executors=$(( executors < max_executors ? executors : max_executors ))
else
  executors=${CUSTOM_EXECUTORS}
fi

if [ "$executors" == "0" -o "$cores_per_executor" == "0" ] ; then
  echo "Could not determine the number of executors and cores per executor"
  exit 1
fi

export CORES_PER_EXECUTOR=${cores_per_executor}

labels="${labels} nproc-${CORES_PER_EXECUTOR}"

# prevent credentials to be seen in the worker environment
su=${SWARM_USER}
# we use a file to pass the password to avoid it showing on `ps`
tmp_file=$(mktemp)
echo "${SWARM_TOKEN}" > ${tmp_file}

unset SWARM_USER
unset SWARM_TOKEN

# make sure the temporary file is removed
(sleep 60 && rm -f ${tmp_file}) &

exec java -jar swarm-client.jar -disableSslVerification -url ${JENKINS_URL} -username ${su} -passwordFile ${tmp_file} \
  -labels "${labels}" -executors ${executors}
