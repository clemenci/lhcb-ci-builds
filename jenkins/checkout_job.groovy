import common.Common

def checkout = job('checkout') {
  concurrentBuild true
  label('checkout')
  wrappers {
    buildName('${task} (#${BUILD_NUMBER})')
    credentialsBinding {
      string {
      	variable("GITLAB_TOKEN")
        credentialsId("lhcbsoft-gitlab-token")
      }
      string {
      	variable("LBTASKS_TOKEN")
        credentialsId("lbtasks-token")
      }
    }
    sshAgent('gitlab-ssh-key')
  }
  steps {
    shell(readFileFromWorkspace('jenkins/scripts/checkout.sh'))
    triggerBuilder {
      configs {
        blockableBuildTriggerConfig {
          projects 'scheduler'
          configs { gitRevisionBuildParameters { combineQueuedCommits false } }
          // FIXME trigger of the scheduler should be unconditional
          configFactories {
            fileBuildParameterFactory {
              filePattern 'data/scheduler_params.txt'
              encoding 'UTF-8'
              noFilesFoundAction 'SKIP'
            }
          }
          // not wanted, but we cannot configure otherwise (dropped later in configuration post-processing)
          block {
            buildStepFailureThreshold ''
            unstableThreshold ''
            failureThreshold ''
          }
        }
      }
    }
  }
  configure { node -> Common.nonBlockingTriggers(node) }
}

Common.setScm(checkout)
Common.addParameters(checkout)
Common.rotateLogs(checkout)
Common.retry(checkout)
