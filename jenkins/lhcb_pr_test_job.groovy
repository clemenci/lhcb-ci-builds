import common.Common

def lhcbpr = job('lhcb-pr-test') {
  authenticationToken(SCHEDULER_TOKEN)
  concurrentBuild true
  parameters {
    string {
      name 'project'
      description 'project to test (<name>/<platform>)'
      trim true
    }
    labelParam("node")
    stringParam("name")
    stringParam("exec_name")
    stringParam("handlers")
    stringParam("runs_count")
  }
  wrappers {
    // FIXME define a nice build name
    // buildName('${task}/${project} (#${BUILD_NUMBER})')
    credentialsBinding {
      string {
      	variable("GITLAB_TOKEN")
        credentialsId("lhcbsoft-gitlab-token")
      }
      string {
      	variable("MATTERMOST_HOOK")
        credentialsId("mattermost-hook")
      }
    }
    buildTimeoutWrapper {
      strategy {
      	absoluteTimeOutStrategy { timeoutMinutes("720") }
      }
      operationList { abortOperation() }
      timeoutEnvVar("JENKINS_JOB_TIMEOUT")
    }
  }
  steps {
    shell(readFileFromWorkspace('jenkins/scripts/lhcb-pr-test.sh'))
  }
  configure { node -> Common.nonBlockingTriggers(node) }
}

Common.setScm(lhcbpr)
Common.addParameters(lhcbpr)
Common.rotateLogs(lhcbpr)
Common.retry(lhcbpr)
