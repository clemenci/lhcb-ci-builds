def seed = job('seed') {
  logRotator{ daysToKeep(15) }
  wrappers {
    credentialsBinding {
      string {
      	variable("START_SLOTS_TOKEN")
        credentialsId("start-slots-token")
      }
      string {
      	variable("SCHEDULER_TOKEN")
        credentialsId("scheduler-token")
      }
    }
  }
  scm {
    git("https://gitlab.cern.ch/lhcb-core/nightly-builds/lhcb-ci-builds.git", "refs/heads/main") { node ->
      // prevent Job DSL implicit perBuildTag
      def ext = node / 'extensions'
      ext.remove( ext.children()[0] )
    }
  }
  label("service")
  triggers {
    scm('@hourly')
  }
  steps {
    dsl(["jenkins/*_job.groovy"])
  }
}
