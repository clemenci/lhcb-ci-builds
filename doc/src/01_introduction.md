# Introduction

The LHCb CI Builds system is a coordinated set of services and utilities to
drive the build the LHCb software stack in different scenarios.

The main components are:
- Tasks and build machines management: Jenkins
  - `schedule` task
  - `checkout` task
  - `build` (and test) task
- Database backend (CouchDB)
  - record build configuration and states
- Web frontend (Flask app on PaaS)
  - summaries and reports from the builds
- Artifacts and logs store (S3)
