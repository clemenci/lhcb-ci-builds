# Tasks

## Interactions between tasks

Jenkins executes tasks on remote machines, but the actual scheduling logic is
handled by the `schedule` task.

The `schedule` task is responsible for preparing the initial state of a given slot
build, then it tells Jenkins to run the next tasks, for example the `checkout`
task.

The `checkout` task clones the repositories of the various projects of the slots
and combine a base commit with the wanted merge requests, then it publishes
the artifacts to the dedicated store. Once done, it tells Jenkins to trigger
another round of scheduling for the current slot build, usually resulting in the
scheduler triggering `build` tasks.

The `build` task builds for one platform one of the projects of the slot. When the
build is over, it publishes the artifacts and tells Jenkins to try to schedule
more work (it might result in other `build` tasks being triggered), then it
runs the tests of the project just built, uploading to the artifacts store the
results of the tests.

This is the sequence diagram of an example slot
```mermaid
sequenceDiagram
    actor User
    participant Jenkins
    participant schedule
    participant checkout
    participant build1 as build
    participant build2 as build

    User ->> Jenkins: start slot
    Jenkins ->>+ schedule: slot
    Note over schedule: prepare db entry for slot/1
    schedule ->>- Jenkins: run checkout for slot/1
    Jenkins ->>+ checkout: slot/1
    checkout ->>- Jenkins: continue slot/1
    Jenkins ->>+ schedule: slot/1
    schedule ->>- Jenkins: run these builds for slot/1
    Jenkins ->>+ build1: slot/1/ProjA
    build1 ->> Jenkins: I'm done building
    Note over build1: now running tests
    Jenkins ->>+ schedule: slot/1
    schedule ->>- Jenkins: run these builds for slot/1
    Jenkins ->>+ build2: slot/1/ProjB
    build2 ->> Jenkins: I'm done building
    Note over build2: now running tests
    Jenkins ->>+ schedule: slot/1
    schedule ->>- Jenkins: nothing to do for slot/1
    Note over build1: tests of slot/1/ProjA completed
    deactivate build1
    Note over build2: tests of slot/1/ProjB completed
    deactivate build2
```
