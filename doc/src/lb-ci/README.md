# lb-ci
```
❯ lb-ci --help
Usage: lb-ci [OPTIONS] COMMAND [ARGS]...

Options:
  -v, --verbose  Increase verbosity
  --help         Show this message and exit.

Commands:
  build                     Build a specific project for a specific...
  check-project-deployment  Check if all dependencies of a project built in...
  checkout                  Checkout all projects declared in a slot,...
  config                    Commands to work with the slots configuration.
  deploy                    Commands to control deployment to CVMFS.
  publish-new-refs          Collect updated reference files from tests in...
  rpm                       Commands to work with RPMs.
  schedule                  Compute what to do next for a given task id in...
  schedule-pr-tests         Get the list of LHCb PR tests to run for a...
  test                      Build a specific project for a specific...
```

## lb-ci build
```
❯ lb-ci build --help
Usage: lb-ci build [OPTIONS] TASK PROJECT_PLATFORM

  Build a specific project for a specific platform (passed as
  'project/platform') as declared in a slot, publishing the archive with the
  build artifacts to an artifacts repository.

Options:
  -j, --processes INTEGER RANGE   Number of parallel processes to use (default
                                  to the number of CPUs)  [x>=1]
  -a, --artifacts-repo ARTIFACTS_REPO_CONFIG
                                  Where the checkout artifacts are stored and
                                  the build artifacts are published
  -w, --workspace DIRECTORY       Where to prepare the workspace layout
  --with-krb5 JSON                Credentials to obtain a Kerberos token as JSON
                                  object with 'principal' and 'keytab' keys
  --help                          Show this message and exit.
```

## lb-ci check-project-deployment
```
❯ lb-ci check-project-deployment --help
Usage: lb-ci check-project-deployment [OPTIONS] TASK PROJECT_PLATFORM

  Check if all dependencies of a project built in a slot are available on CVMFS.

Options:
  --help  Show this message and exit.
```

## lb-ci checkout
```
❯ lb-ci checkout --help
Usage: lb-ci checkout [OPTIONS] TASK

  Checkout all projects declared in a slot, applying the needed merge requests,
  optionally publishing the archives with the code to an artifacts repository.

Options:
  -j, --processes INTEGER RANGE   Number of parallel processes to use (default
                                  to the number of CPUs)  [x>=1]
  -a, --artifacts-repo ARTIFACTS_REPO_CONFIG
                                  Where to store checkout artifacts
  -w, --workspace DIRECTORY       Where to prepare the workspace layout
  --help                          Show this message and exit.
```

## lb-ci config
```
❯ lb-ci config --help
Usage: lb-ci config [OPTIONS] COMMAND [ARGS]...

  Commands to work with the slots configuration.

Options:
  --help  Show this message and exit.

Commands:
  check-preconditions  Check the preconditions of the given slot build.
  ci-test              Generate the slot configuration(s) for a CI test.
  get-json             Get the configuration JSONI object of the given slot...
  get-release-slots    Get from the lhcbstacks repository the list of...
  get-slots            Get the list of JSON documents with the...
  publish              Publish a list of slot configurations as new builds...
  remove-duplicates    Remove from a list of slots those identical to the...
  resolve-mrs          Given a list of slot configurations as inputs,...
  update-dependencies  Update the configuration of a slot in the database...
```

### lb-ci config check-preconditions
```
❯ lb-ci config check-preconditions --help
Usage: lb-ci config check-preconditions [OPTIONS] TASK PLATFORM

  Check the preconditions of the given slot build.

  Updates the DB and returns one of the following values: - ok - not-ok -
  timeout

  For example: ``` task=flavour/slot-name/123 platform=some-platform while true
  ; do     case $(lb-ci config check-preconditions ${task} ${platform}) in
  ok) break ;; # all good       not-ok) ;; # wait more       timeout) exit 1 ;;
  # enough waiting     esac     sleep 60 done ```

Options:
  --start-timestamp INTEGER  Timestamp of the first iteration of the check loop,
                             overrides the content of the DB
  --timeout INTEGER          Timeout in seconds to override what defined in the
                             configuration
  --help                     Show this message and exit.
```

### lb-ci config ci-test
```
❯ lb-ci config ci-test --help
Usage: lb-ci config ci-test [OPTIONS] [SOURCES]...

  Generate the slot configuration(s) for a CI test.

Options:
  --platforms TEXT                Platforms to run the tests on
  --merge / --no-merge            Whether to merge the changes
  --build-reference / --no-build-reference
                                  Whether to build the reference
  --with-downstream / --no-downstream
                                  Whether to build/test downstream projects
  --model TEXT                    Model slot to use for the tests
  --trigger JSON                  JSON object with details about the ci-test
                                  trigger message
  -o, --output FILENAME           Where to write the output to (default to
                                  stdout)
  --config-repo TEXT              Where to get the slots configuration from,
                                  either a git URL with optional `#commit-ish`,
                                  just `#commit-ish` or path to a directory
  --from-file FILENAME            Read the sources and options from a property
                                  file like the one generated by lbn-gitlab-mr
  --help                          Show this message and exit.
```

### lb-ci config get-json
```
❯ lb-ci config get-json --help
Usage: lb-ci config get-json [OPTIONS] TASK

  Get the configuration JSONI object of the given slot build.

Options:
  -o, --output FILENAME  Where to write the output to (default to stdout)
  --help                 Show this message and exit.
```

### lb-ci config get-release-slots
```
❯ lb-ci config get-release-slots --help
Usage: lb-ci config get-release-slots [OPTIONS]

  Get from the lhcbstacks repository the list of pending release stacks.

Options:
  -o, --output FILENAME         Where to write the output to (default to stdout)
  -f, --flavour TEXT            Database partition where the release slots are
                                stored
  --lhcbstacks TEXT             Where to get the release stack requests from
                                [default: https://gitlab.cern.ch/lhcb-
                                core/lhcbstacks]
  --update-db / --no-update-db  Whether to record in the database the list of
                                managed stacks
  --help                        Show this message and exit.
```

### lb-ci config get-slots
```
❯ lb-ci config get-slots --help
Usage: lb-ci config get-slots [OPTIONS] [SLOTS_NAMES]...

  Get the list of JSON documents with the configuration of the requested slots.

  If no slot name is provided, all enabled slots are includes, unless the --all-
  slot option is passed.

Options:
  -o, --output FILENAME  Where to write the output to (default to stdout)
  --config-repo TEXT     Where to get the slots configuration from, either a git
                         URL with optional `#commit-ish`, just `#commit-ish` or
                         path to a directory
  -a, --all-slots        If specified, process all defined slots and not only
                         the enabled ones
  --help                 Show this message and exit.
```

### lb-ci config publish
```
❯ lb-ci config publish --help
Usage: lb-ci config publish [OPTIONS] [INPUT]

  Publish a list of slot configurations as new builds to the dashboard database.

  Each slot from the input list gets a new build id when published and we write
  to output the id of the newly declared builds, one per line (as
  `<flavour>/<name>/<build_id>`).

  If a CI test slot is detected, we resolve and update the reference build id in
  the slot config metadata.

Options:
  -o, --output FILENAME
  -f, --flavour TEXT     Database partition to add the slot to
  -d, --date [%Y-%m-%d]  Reference date to use for the builds
  -u, --user TEXT        user name to connect to the DB
  -p, --password TEXT    password or token to connect to the DB
  --help                 Show this message and exit.
```

### lb-ci config remove-duplicates
```
❯ lb-ci config remove-duplicates --help
Usage: lb-ci config remove-duplicates [OPTIONS] [INPUT]

  Remove from a list of slots those identical to the latest build in the
  database.

Options:
  -o, --output FILENAME
  -f, --flavour TEXT           Database partition to use to check for duplicates
  -d, --date [%Y-%m-%d]        Reference date to compute age of latest build
  -a, --max-age INTEGER RANGE  Maximum number of days before we have to force a
                               rebuild (default 7, 0 means always rebuild,
                               overridden by max_build_age field in the slot
                               config metadata)  [x>=0]
  -p, --progress               Display a progress bar while processing
  --help                       Show this message and exit.
```

### lb-ci config resolve-mrs
```
❯ lb-ci config resolve-mrs --help
Usage: lb-ci config resolve-mrs [OPTIONS] [INPUT]

  Given a list of slot configurations as inputs, produce an updated
  configuration where the checkout options are extended to include explicit
  merge actions for the required merge requests.

  By default input and output point to stdin and stdout, but they can be
  overridden to use files.

Options:
  -o, --output FILENAME
  -p, --progress                 Display a progress bar while processing
  -j, --processes INTEGER RANGE  Number of parallel processes to use (default to
                                 the number of CPUs)  [x>=1]
  --help                         Show this message and exit.
```

### lb-ci config update-dependencies
```
❯ lb-ci config update-dependencies --help
Usage: lb-ci config update-dependencies [OPTIONS] TASK

  Update the configuration of a slot in the database with the dependencies that
  are extracted from the code in the workspace.

  For ci-test slots, we also disable non-relevant tests and projects.

Options:
  -w, --workspace DIRECTORY  Where the sources have been checkout out to
  --help                     Show this message and exit.
```

## lb-ci deploy
```
❯ lb-ci deploy --help
Usage: lb-ci deploy [OPTIONS] COMMAND [ARGS]...

  Commands to control deployment to CVMFS.

Options:
  --help  Show this message and exit.

Commands:
  build      Prepare an archive with the InstallArea/platform of a project...
  structure  Prepare an archive with the structure of the deployment of the...
```

### lb-ci deploy build
```
❯ lb-ci deploy build --help
Usage: lb-ci deploy build [OPTIONS] TASK PROJECT_PLATFORM

  Prepare an archive with the InstallArea/platform of a project built in a slot
  and deploy it to CVMFS.

Options:
  -a, --artifacts-repo ARTIFACTS_REPO_CONFIG
                                  Where to store artifacts
  -w, --workspace DIRECTORY       Where the project was copied and built
  --help                          Show this message and exit.
```

### lb-ci deploy structure
```
❯ lb-ci deploy structure --help
Usage: lb-ci deploy structure [OPTIONS] TASK

  Prepare an archive with the structure of the deployment of the slot and deploy
  it to CVMFS.

  The structure includes checked out sources and empty binary (InstallArea)
  directories for all the expected platforms, so this command must be run after
  a checkout.

  The main goal is to, while building the first projects, prepare in CVMFS a
  structure that can speed up deployment of the binary artifacts (by means of
  partial locks of the directories).

Options:
  -a, --artifacts-repo ARTIFACTS_REPO_CONFIG
                                  Where to store artifacts
  -w, --workspace DIRECTORY       Where the sources have been checkout out to
  --help                          Show this message and exit.
```

## lb-ci publish-new-refs
```
❯ lb-ci publish-new-refs --help
Usage: lb-ci publish-new-refs [OPTIONS] TASK PROJECT_PLATFORM

  Collect updated reference files from tests in the source tree (`.new` files),
  pack them in a ZIP file and upload it to the artifacts repository.

Options:
  -a, --artifacts-repo ARTIFACTS_REPO_CONFIG
                                  Where to store artifacts
  -w, --workspace DIRECTORY       Where the project was copied and tested
  --help                          Show this message and exit.
```

## lb-ci rpm
```
❯ lb-ci rpm --help
Usage: lb-ci rpm [OPTIONS] COMMAND [ARGS]...

  Commands to work with RPMs.

Options:
  --help  Show this message and exit.

Commands:
  make-bin    Generate and publish RPMs for the projects binary files in a...
  make-index  Generate and publish RPMs for the glimpse index of projects a...
  make-src    Generate and publish RPMs for the projects source files in a...
```

### lb-ci rpm make-bin
```
❯ lb-ci rpm make-bin --help
Usage: lb-ci rpm make-bin [OPTIONS] TASK PROJECT_PLATFORM

  Generate and publish RPMs for the projects binary files in a release slot.

  Nothing is done if the slot is not a release slot (unless --force is used).

Options:
  -a, --artifacts-repo ARTIFACTS_REPO_CONFIG
                                  Where the checkout artifacts are stored and
                                  the build artifacts are published
  -w, --workspace DIRECTORY       Where to prepare the workspace layout
  --force                         Generate the binary RPM even for non-release
                                  slots
  --help                          Show this message and exit.
```

### lb-ci rpm make-index
```
❯ lb-ci rpm make-index --help
Usage: lb-ci rpm make-index [OPTIONS] TASK PROJECT_PLATFORM

  Generate and publish RPMs for the glimpse index of projects a release slot.

  Nothing is done if the slot is not a release slot or the platform is not the
  first of those listed in the configuration. The index requires binary
  artifacts, but it does not matter which platform is used as long as it is
  produced exactly once. The option --force can be used to generate the index in
  any case, for debugging purposes.

Options:
  -a, --artifacts-repo ARTIFACTS_REPO_CONFIG
                                  Where the checkout artifacts are stored and
                                  the build artifacts are published
  -w, --workspace DIRECTORY       Where to prepare the workspace layout
  --force                         Generate the index RPM even for non-release
                                  slots or non-first platforms
  --help                          Show this message and exit.
```

### lb-ci rpm make-src
```
❯ lb-ci rpm make-src --help
Usage: lb-ci rpm make-src [OPTIONS] TASK

  Generate and publish RPMs for the projects source files in a release slot.

  Nothing is done if the slot is not a release slot (unless --force is used).

Options:
  -j, --processes INTEGER RANGE   Number of parallel processes to use (default
                                  to the number of CPUs)  [x>=1]
  -a, --artifacts-repo ARTIFACTS_REPO_CONFIG
                                  Where to store checkout artifacts
  -w, --workspace DIRECTORY       Where to prepare the workspace layout
  --force                         Generate the source RPM even for non-release
                                  slots
  --help                          Show this message and exit.
```

## lb-ci schedule
```
❯ lb-ci schedule --help
Usage: lb-ci schedule [OPTIONS] TASK

  Compute what to do next for a given task id in the form
  `<flavour>/<slot>/<build_id>`.

  The output of the script is (optionally) a number of Jenkins job property
  files in the directory specified by --next-tasks-dir

Options:
  --next-tasks-dir DIRECTORY  where the next jobs property files have to be
                              created
  --help                      Show this message and exit.
```

## lb-ci schedule-pr-tests
```
❯ lb-ci schedule-pr-tests --help
Usage: lb-ci schedule-pr-tests [OPTIONS] TASK PROJECT_PLATFORM

  Get the list of LHCb PR tests to run for a given slot, project, platform.

  The output is (optionally) a number of Jenkins job property files in the
  directory specified by --next-tasks-dir

Options:
  --next-tasks-dir DIRECTORY  where the next jobs property files have to be
                              created
  --help                      Show this message and exit.
```

## lb-ci test
```
❯ lb-ci test --help
Usage: lb-ci test [OPTIONS] TASK PROJECT_PLATFORM

  Build a specific project for a specific platform (passed as
  'project/platform') as declared in a slot, publishing the archive with the
  build artifacts to an artifacts repository.

Options:
  -j, --processes INTEGER RANGE   Number of parallel processes to use (default
                                  to the number of CPUs)  [x>=1]
  -a, --artifacts-repo ARTIFACTS_REPO_CONFIG
                                  Where the checkout artifacts are stored and
                                  the build artifacts are published
  -w, --workspace DIRECTORY       Where to prepare the workspace layout
  --with-krb5 JSON                Credentials to obtain a Kerberos token as JSON
                                  object with 'principal' and 'keytab' keys
  --help                          Show this message and exit.
```
