from click.testing import CliRunner

from lb.jenkins import lb_ci


def _fmt_help(cmd, help):
    level = len(cmd)
    cmd = " ".join(cmd)
    return f"""{'#' * level} {cmd}
```
❯ {cmd} --help
{help.rstrip()}
```
"""


def generate_readme(command_path=[lb_ci]):
    runner = CliRunner()
    cmd = [c.name for c in command_path]
    result = runner.invoke(command_path[0], cmd[1:] + ["--help"])
    if len(cmd) > 1:
        print()  # empty line between sections, but not before the first
    print(_fmt_help(cmd, result.stdout), end="")
    if hasattr(command_path[-1], "commands"):
        for subcommand in sorted(
            command_path[-1].commands.values(), key=lambda c: c.name
        ):
            generate_readme(command_path=command_path + [subcommand])


if __name__ == "__main__":
    generate_readme()
