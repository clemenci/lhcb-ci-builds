import contextlib
import json
import logging
import os.path
import subprocess
import sys
from collections import namedtuple
from copy import deepcopy
from datetime import date, datetime, timedelta
from io import StringIO
from multiprocessing import Pool
from pathlib import Path
from socket import gethostname
from tempfile import TemporaryDirectory
from typing import Mapping, Optional
from unittest.mock import patch

import click
import couchdb
import yaml
from LbNightlyTools.Configuration import DataProject, Project, Slot, loadConfig
from LbNightlyTools.GitlabUtils import getMRLabels, resolveMRs
from LbNightlyTools.MergeRequestBuilds import (
    create_mr_slots,
    post_exception_to_gitlab,
    post_gitlab_feedback,
)
from LbNightlyTools.Scripts import Preconditions
from LbNightlyTools.Utils import Dashboard

from .ci import lb_ci
from .common import TaskId, temp_env, update_db

DEFAULT_CONF_REPO = "https://gitlab.cern.ch/lhcb-core/LHCbNightlyConf.git"
LHCBSTACKS_REPO = "https://gitlab.cern.ch/lhcb-core/lhcbstacks"
# FIXME frontend uses "stacks2", but there is a "stacks"... what should we do?
LHCBSTACKS_RECORD_DOC = "lhcbstacks"

logger = logging.getLogger(__name__)


@lb_ci.group()
def config():
    """
    Commands to work with the slots configuration.
    """
    pass


@config.command()
@click.argument("slots_names", nargs=-1)
@click.option(
    "-o",
    "--output",
    default="-",
    type=click.File("w"),
    help="Where to write the output to (default to stdout)",
)
@click.option(
    "--config-repo",
    help="Where to get the slots configuration from, either a git URL with optional "
    "`#commit-ish`, just `#commit-ish` or path to a directory",
)
@click.option(
    "-a",
    "--all-slots",
    is_flag=True,
    help="If specified, process all defined slots and not only the enabled ones",
)
def get_slots(
    slots_names: list[str], output, config_repo: Optional[str], all_slots: bool
):
    """
    Get the list of JSON documents with the configuration of the requested slots.

    If no slot name is provided, all enabled slots are includes, unless the
    --all-slot option is passed.
    """
    if not config_repo:
        config_repo = DEFAULT_CONF_REPO
    elif config_repo.startswith("#"):
        config_repo = DEFAULT_CONF_REPO + config_repo

    slots = get_slot_configs(config_repo)
    all_names = {slot.name for slot in slots}
    if slots_names:
        missing_names = set(slots_names).difference(all_names)
        if missing_names:
            raise click.BadParameter(
                "the following slot names are not known:"
                f" {', '.join(sorted(missing_names))}"
            )
        slots = [slot for slot in slots if slot.name in slots_names]
    elif not all_slots:
        slots = [slot for slot in slots if slot.enabled]

    slots.sort(key=lambda slot: slot.name)
    json.dump([slot.toDict() for slot in slots], output, indent=2, sort_keys=True)


def get_slot_configs(repository: str) -> list[Slot]:
    """
    Get all slot defined in the LHCbNightlyConf repository (Git URL or local path).
    """
    # we print the output of git commands only in debug
    capture_output = logging.getLogger().level != logging.DEBUG

    def run(cmd, **kwargs):
        logger.debug("running: %s %s", cmd, kwargs)
        return subprocess.run(cmd, check=True, capture_output=capture_output, **kwargs)

    commit = None
    if "#" in repository:
        repository, commit = repository.split("#", 1)
    if os.path.isdir(repository):
        # it's an existing directory, so we use the files from there
        logger.info("using config from directory %s", repository)
        repository_path = Path(repository)
    else:
        logger.info(
            "using config from repository %s%s",
            repository,
            f" (commit {commit})" if commit else "",
        )
        tmp_dir = TemporaryDirectory()
        repository_path = tmp_dir.name
        run(["git", "clone", repository, repository_path])
        if commit:
            run(["git", "checkout", commit], cwd=repository_path)
    # loadConfig must be run from the repository directory
    with contextlib.chdir(repository_path):
        return list(loadConfig().values())


def setSlotLabelsFromMRs(slot: Slot) -> Slot:
    """
    Store in slot.metadata["labels"] all labels set to the used MRs
    of the active projects.
    """
    # Collect all labels from all MRs, to be used by LHCbPR,
    # see LbNightlyTools#109
    # (code copied from LbNightlyTools.Scripts.EnabledSlots because
    # there was not a function for the task)
    all_labels = set()
    logger.debug("collect all labels from MRs of %s", slot.name)
    for project in slot.activeProjects:
        merges = project.checkout_opts.get("merges", [])
        if not isinstance(merges, list):
            merges = [merges]
        if merges:
            logger.debug("getting labels for project %s", project.name)
        for mr_desc in merges:
            if len(mr_desc) > 1:
                mr_iid = mr_desc[0]
                if isinstance(mr_iid, int):
                    all_labels.update(getMRLabels(project, mr_iid))
    slot.metadata["labels"] = sorted(all_labels)
    logger.debug("labels: %s", slot.metadata["labels"])
    return slot


class MrResolver:
    """
    Wrapper around LbNightlyTools.GitlabUtils.resolveMRs that can be used with
    multiprocessing.
    """

    def __init__(self, with_log=True):
        self.with_log = with_log

    def log(self, *args, **kwargs):
        if self.with_log:
            logger.info(*args, **kwargs)

    def __call__(self, slot_dict: dict) -> dict:
        self.log(
            "resolving merge requests and labels for %s",
            slot_dict.get("slot", "unknown"),
        )
        slot: Slot = resolveMRs(Slot.fromDict(slot_dict))
        slot = setSlotLabelsFromMRs(slot)
        return slot.toDict()


@config.command()
@click.argument("input", type=click.File("r"), default="-")
@click.option("-o", "--output", type=click.File("w"), default="-")
@click.option(
    "-p", "--progress", is_flag=True, help="Display a progress bar while processing"
)
@click.option(
    "-j",
    "--processes",
    type=click.IntRange(min=1),
    help="Number of parallel processes to use (default to the number of CPUs)",
)
def resolve_mrs(input, output, progress, processes):
    """
    Given a list of slot configurations as inputs, produce
    an updated configuration where the checkout options are extended
    to include explicit merge actions for the required merge requests.

    By default input and output point to stdin and stdout, but they
    can be overridden to use files.
    """
    if progress and logging.getLogger().level == logging.DEBUG:
        logger.debug("disabling progress bar with logging level set to debug")
        progress = False

    slots_input = json.load(input)

    pool = Pool(processes)
    with click.progressbar(
        pool.imap_unordered(
            MrResolver(with_log=not progress),
            slots_input,
        ),
        label="resolving MRs",
        length=len(slots_input),
        item_show_func=lambda slot: slot.get("slot") if slot else None,
        # this prevent printout if we do not want a progress bar
        file=sys.stderr if progress else StringIO(),
    ) as bar:
        slots_output = list(bar)

    slots_output.sort(key=lambda item: item.get("slot", ""))
    json.dump(slots_output, output, indent=2, sort_keys=True)


class ToBuild:
    """
    Helper to compare a slot configuration dictionary with the
    latest build in the dashboard.
    """

    def __init__(self, flavour, ref_date, max_age):
        self.flavour = flavour
        self.ref_date = ref_date
        self.max_age = max_age
        self._dashboard = None

    @property
    def dashboard(self):
        if self._dashboard is None:
            self._dashboard = Dashboard(flavour=self.flavour)
        return self._dashboard

    def clean_slot_dict(self, slot_dict):
        """
        Clean up slot config dictionary for clean comparison.
        """
        if "build_id" in slot_dict:
            del slot_dict["build_id"]
        for p in slot_dict.get("projects", []):
            if "dependencies" in p:  # these are generated during checkout
                del p["dependencies"]
        slot_dict.get("projects", []).sort(key=lambda p: p.get("name"))
        slot_dict.get("packages", []).sort(key=lambda p: p.get("name"))
        slot_dict.setdefault("metadata", {})
        for metadata_to_ignore in ["config_id", "flavour"]:
            if metadata_to_ignore in slot_dict["metadata"]:
                del slot_dict["metadata"][metadata_to_ignore]
        if (
            "ci_test" in slot_dict["metadata"]
            and "trigger" in slot_dict["metadata"]["ci_test"]
        ):
            del slot_dict["metadata"]["ci_test"]["trigger"]

    def __call__(self, slot_dict):
        """
        Return True is slot_dict is different enough wrt the latest build
        in the dashboard to justify a build.
        """
        # avoid changes to the object we are given
        slot_dict = deepcopy(slot_dict)

        def to_date(s: str) -> date:
            "convert a string YYYY-MM-DD to a date object"
            return datetime.strptime(s, "%Y-%m-%d").date()

        name = slot_dict["slot"]
        logger.debug("check if to rebuild %s", name)

        build_id = self.dashboard.lastBuildId(name)
        last_slot_info = self.dashboard.db.get(f"{name}.{build_id}", {})

        if last_slot_info.get("aborted"):
            logger.debug("  previous build was aborted")
            return True

        # check previous build age
        age = (self.ref_date - to_date(last_slot_info.get("date", "1970-01-01"))).days
        age_limit = slot_dict.get("metadata", {}).get("max_build_age", self.max_age)
        logger.debug("  age: %d (limit %d)", age, age_limit)
        if age >= age_limit:
            return True

        last_dict = last_slot_info.get("config", {})

        # check for differences
        # (drop elements not to be compared)
        self.clean_slot_dict(slot_dict)
        self.clean_slot_dict(last_dict)

        # convert to JSON for clean comparison
        last_json = json.dumps(last_dict, indent=2, sort_keys=True)
        slot_json = json.dumps(slot_dict, indent=2, sort_keys=True)
        if last_json != slot_json:
            logger.debug("  changed config:")
            from difflib import context_diff

            for line in context_diff(
                last_json.splitlines(True),
                slot_json.splitlines(True),
                fromfile="before",
                tofile="after",
            ):
                logger.debug(line.rstrip())
            return True
        else:
            logger.debug("  unchanged config")
            return False


@config.command()
@click.argument("input", type=click.File("r"), default="-")
@click.option("-o", "--output", type=click.File("w"), default="-")
@click.option(
    "-f",
    "--flavour",
    default="nightly",
    help="Database partition to use to check for duplicates",
)
@click.option(
    "-d",
    "--date",
    default=datetime.now(),
    type=click.DateTime(["%Y-%m-%d"]),
    help="Reference date to compute age of latest build",
)
@click.option(
    "-a",
    "--max-age",
    default=7,
    type=click.IntRange(min=0),
    help="Maximum number of days before we have to force a "
    "rebuild (default 7, 0 means always rebuild, overridden "
    "by max_build_age field in the slot config metadata)",
)
@click.option(
    "-p", "--progress", is_flag=True, help="Display a progress bar while processing"
)
def remove_duplicates(input, output, flavour, date, max_age, progress):
    """
    Remove from a list of slots those identical to the latest build in
    the database.
    """
    slots_input = json.load(input)
    slots_to_build = []
    slots_dropped = []

    logger.info("checking which slots have to be rebuilt")
    to_build = ToBuild(flavour, date.date(), max_age)
    with click.progressbar(
        slots_input,
        length=len(slots_input),
        item_show_func=lambda slot: slot.get("slot") if slot else None,
        # this prevent printout if we do not want a progress bar
        file=sys.stderr if progress else StringIO(),
    ) as bar:
        for slot in bar:
            if to_build(slot):
                slots_to_build.append(slot)
            else:
                slots_dropped.append(slot)

    logger.info(
        "%d slots kept, %s slots dropped", len(slots_to_build), len(slots_dropped)
    )
    if slots_dropped:
        logger.info(
            "dropped slots: %s", ", ".join(slot["slot"] for slot in slots_dropped)
        )
    json.dump(slots_to_build, output, indent=2, sort_keys=True)


@config.command()
@click.argument("input", type=click.File("r"), default="-")
@click.option("-o", "--output", type=click.File("w"), default="-")
@click.option(
    "-f",
    "--flavour",
    default="nightly",
    help="Database partition to add the slot to",
)
@click.option(
    "-d",
    "--date",
    default=datetime.now(),
    type=click.DateTime(["%Y-%m-%d"]),
    help="Reference date to use for the builds",
)
@click.option("-u", "--user", help="user name to connect to the DB")
@click.option("-p", "--password", help="password or token to connect to the DB")
def publish(input, output, flavour, user, password, date):
    """
    Publish a list of slot configurations as new builds to the dashboard database.

    Each slot from the input list gets a new build id when published and we write
    to output the id of the newly declared builds, one per line
    (as `<flavour>/<name>/<build_id>`).

    If a CI test slot is detected, we resolve and update the reference
    build id in the slot config metadata.
    """
    BasicSlotInfo = namedtuple("BasicSlotInfo", ["name", "build_id"])

    if user and password:
        credentials = (user, password)
    elif user or password:
        raise click.BadParameter(
            "both --user and --password must be specified, or none of them"
        )
    else:
        credentials = None

    slots = json.load(input)
    # Make sure we keep the ci-test slots at the end (so that we can
    # update the reference build id if the reference slot is in the
    # list). This works because False < True.
    slots.sort(
        key=lambda slot: "reference" in slot.get("metadata", {}).get("ci_test", {})
    )

    d = Dashboard(credentials=credentials, flavour=flavour)

    # remember the build ids produced so far
    build_ids = {}
    for slot in slots:
        slot["metadata"]["flavour"] = flavour
        name = slot["slot"]
        from couchdb import ResourceConflict

        while True:
            try:
                if "reference" in slot.get("metadata", {}).get("ci_test", {}):
                    # resolve the reference build id
                    ref_name = slot["metadata"]["ci_test"]["reference"][0]
                    if ref_name in build_ids:
                        ref_build_id = build_ids[ref_name]
                    else:
                        ref_build_id = d.lastBuildId(ref_name)
                        build_ids[ref_name] = ref_build_id

                    slot["metadata"]["ci_test"]["reference"] = (
                        ref_name,
                        ref_build_id,
                    )

                slot["build_id"] = build_id = d.lastBuildId(name) + 1
                doc_name = f"{name}.{build_id}"
                doc = {
                    "_id": doc_name,
                    "type": "slot-info",
                    "slot": name,
                    "build_id": build_id,
                    "date": str(date.date()),
                    "created": str(datetime.now()),
                    "config": slot,
                }
                d.db.save(doc)  # this may raise ResourceConflict
                output.write(f"{flavour}/{name}/{build_id}\n")
                build_ids[name] = build_id

                # FIXME we need to improve how we publish ci-test feedback to gitlab
                if "reference" in slot.get("metadata", {}).get("ci_test", {}):
                    ref_slot = BasicSlotInfo(*slot["metadata"]["ci_test"]["reference"])
                    test_slot = BasicSlotInfo(name=name, build_id=build_id)
                    post_gitlab_feedback(
                        ref_slot,
                        test_slot,
                        flavour,
                        slot["metadata"]["ci_test"]["config"],
                    )
                break  # we successfully created the doc, no need to retry
            except ResourceConflict:
                # somebody already created the doc with the same _id
                # let's try again
                pass


@config.command()
@click.argument("task", type=TaskId.from_str)
@click.option(
    "-w",
    "--workspace",
    type=click.Path(dir_okay=True, file_okay=False),
    default="workspace",
    help="Where the sources have been checkout out to",
)
def update_dependencies(task: TaskId, workspace: Path):
    """
    Update the configuration of a slot in the database with
    the dependencies that are extracted from the code in the
    workspace.

    For ci-test slots, we also disable non-relevant tests
    and projects.
    """
    # FIXME deduplicate
    d = Dashboard(flavour=task.flavour)
    doc_id = task.doc_id()
    try:
        doc = d[doc_id]
    except couchdb.ResourceNotFound:
        exit(f"unknown build {task}")

    doc_orig = deepcopy(doc)
    slot = Slot.fromDict(doc["config"])

    # list of dependencies that have to be added to the configuration
    # because they are not regular projects
    mandatory_deps = ["lcg-toolchains"]
    projects_names = set(project.name for project in slot.projects)

    def fix_case(name: str) -> str:
        "fix the case of a project name"
        for candidate in projects_names:
            if name.lower() == candidate.lower():
                return candidate
        return name

    # chdir is needed only to allow the fallback to Project.dependencies
    with contextlib.chdir(workspace):
        for project in slot.activeProjects:
            if isinstance(project, DataProject):
                # data projects do not have dependencies
                continue

            # get the project dictionary from the configuration
            project_dict = {}
            for project_dict in doc["config"]["projects"]:
                if project_dict["name"] == project.name:
                    break

            metadata_file = Path(project.baseDir) / "lhcbproject.yml"

            if metadata_file.exists():
                deps = yaml.safe_load(open(metadata_file)).get("dependencies", [])
            else:
                logger.warning(
                    "%s: dependencies scan not supported without lhcbproject.yaml, "
                    "falling back to LbNightlyTools",
                    project.name,
                )
                deps = project.dependencies()
            deps.extend(mandatory_deps)
            deps.extend(project_dict.get("dependencies", []))
            # keep only the projects that are in the active list (and deduplicate)
            deps = sorted(
                set(fix_case(name) for name in deps).intersection(projects_names)
                - {project.name}
            )
            project_dict["dependencies"] = deps

    keep_needed(doc["config"])

    if doc == doc_orig:
        logger.info("slot document not changed, DB already up to date")
    else:
        logger.info("recording projects' dependencies in the database")
        # FIXME this will raise an exception if the slot is aborted while we work
        d.db.save(doc)


def keep_needed(config: Mapping):
    """
    If it's a ci-test slot, modify config to disable all projects
    that are not needed and turn off tests that cannot be affected
    by the changes.
    """
    from itertools import chain

    from networkx.algorithms.dag import ancestors, descendants

    slot = Slot.fromDict(config)

    if not (
        "ci_test" in slot.metadata
        and slot.metadata["ci_test"].get("is_test")
        and slot.metadata["ci_test"].get("requested_projects")
    ):
        return

    requested_projects = slot.metadata["ci_test"]["requested_projects"]
    with_downstream = slot.metadata["ci_test"].get("with_downstream", True)

    deps = slot.dependencyGraph()
    # - we have to build the projects explicitly listed
    #   (excluding data packages)
    needed = set(requested_projects) & set(deps)
    # - plus all projects that use the listed ones (if requested)
    if with_downstream:
        needed.update(list(chain.from_iterable(descendants(deps, p) for p in needed)))
    # - test projects that changed or depend on those that changed
    need_testing = set(needed)
    # - and all projects that are needed to build those we need
    needed.update(list(chain.from_iterable(ancestors(deps, p) for p in needed)))
    # - disable everything else
    #   (making sure we do not enable something already disabled)
    for p in config["projects"]:
        if not p["disabled"]:
            n = p["name"]
            p["disabled"] = n not in needed
            # FIXME same logic as in LbNightlyTools, but is it correct?
            p["no_test"] = n not in need_testing

    logger.debug("projects to build: %s", needed)
    logger.debug("projects to test: %s", need_testing)


@config.command()
@click.argument("sources", nargs=-1)
@click.option("--platforms", multiple=True, help="Platforms to run the tests on")
@click.option("--merge/--no-merge", default=True, help="Whether to merge the changes")
@click.option(
    "--build-reference/--no-build-reference",
    default=False,
    help="Whether to build the reference",
)
@click.option(
    "--with-downstream/--no-downstream",
    default=True,
    help="Whether to build/test downstream projects",
)
@click.option("--model", help="Model slot to use for the tests")
@click.option(
    "--trigger",
    metavar="JSON",
    type=json.loads,
    help="JSON object with details about the ci-test trigger message",
)
@click.option(
    "-o",
    "--output",
    default="-",
    type=click.File("w"),
    help="Where to write the output to (default to stdout)",
)
@click.option(
    "--config-repo",
    help="Where to get the slots configuration from, either a git URL with optional "
    "`#commit-ish`, just `#commit-ish` or path to a directory",
)
@click.option(
    "--from-file",
    type=click.File("r"),
    help="Read the sources and options from a property file like the one generated by "
    "lbn-gitlab-mr",
)
def ci_test(
    sources,
    platforms,
    merge,
    build_reference,
    with_downstream,
    model,
    output,
    config_repo,
    from_file,
    trigger,
):
    """
    Generate the slot configuration(s) for a CI test.
    """
    if not config_repo:
        config_repo = DEFAULT_CONF_REPO
    elif config_repo.startswith("#"):
        config_repo = DEFAULT_CONF_REPO + config_repo

    if from_file:
        data = from_file.read()
        prefix = "MR_TOKEN="
        if not data.startswith(prefix):
            raise ValueError(f"invalid file format, it should start with {prefix!r}")
        mr_slots_config = json.loads(data[len(prefix) :])
        sources = mr_slots_config.get("sources", sources)
        platforms = mr_slots_config.get("platforms", platforms)
        merge = mr_slots_config.get("merge", merge)
        build_reference = mr_slots_config.get("build_reference", build_reference)
        with_downstream = mr_slots_config.get("with_downstream", with_downstream)
        model = mr_slots_config.get("model", model)
        trigger = mr_slots_config.get("trigger", trigger)
    else:
        mr_slots_config = dict(
            sources=sources,
            platforms=platforms,
            merge=merge,
            build_reference=build_reference,
            with_downstream=with_downstream,
            model=model,
            trigger=trigger,
        )

    slots = get_slot_configs(config_repo)

    try:
        # Call the create_mr_slots main method
        ref_slot, test_slot = create_mr_slots(
            sources=sources,
            platforms=platforms,
            merge=merge,
            model_slots=slots,
            with_downstream=with_downstream,
            force_model=model,
        )
        test_slot.metadata["ci_test"]["config"] = mr_slots_config
    except Exception as err:
        logger.error("failed to create MR slots: %s", err)
        post_exception_to_gitlab({"trigger": trigger}, err)
        logger.info("not triggering builds")
        exit(1)

    # register trigger information.
    # Needed for the GitLab reply of the LHCbPR2HD ThroughputProfileHandler.py
    ref_slot.metadata["ci_test"]["trigger"] = trigger
    test_slot.metadata["ci_test"]["trigger"] = trigger
    # This is to be filled later on
    test_slot.metadata["ci_test"]["reference"] = (
        ref_slot.name if build_reference else test_slot.metadata["ci_test"]["model"],
        0,
    )

    slots = [ref_slot, test_slot] if build_reference else [test_slot]

    json.dump([slot.toDict() for slot in slots], output, indent=2, sort_keys=True)


@config.command()
@click.option(
    "-o",
    "--output",
    default="-",
    type=click.File("w"),
    help="Where to write the output to (default to stdout)",
)
@click.option(
    "-f",
    "--flavour",
    default="nightly",
    help="Database partition where the release slots are stored",
)
@click.option(
    "--lhcbstacks",
    default=LHCBSTACKS_REPO,
    show_default=True,
    help="Where to get the release stack requests from",
)
@click.option(
    "--update-db/--no-update-db",
    "update_db_doc",
    default=True,
    help="Whether to record in the database the list of managed stacks",
)
def get_release_slots(output, lhcbstacks, flavour, update_db_doc):
    """
    Get from the lhcbstacks repository the list of pending release stacks.
    """
    all_stacks = get_requested_stacks(lhcbstacks)
    click.echo(f"Found {len(all_stacks)} stacks from merge requests")

    d = Dashboard(flavour=flavour)
    db = d.db
    old_stacks = db.get(LHCBSTACKS_RECORD_DOC) or {"value": []}

    stacks = [stack for stack in all_stacks if stack not in old_stacks["value"]]
    print(f"Found {len(stacks)} stacks to build")
    if update_db_doc:
        update_db(
            d.db,
            LHCBSTACKS_RECORD_DOC,
            lambda doc: doc.update({"type": "stacks", "value": all_stacks}),
        )
    else:
        click.echo("Not updating the database:")
        click.echo(json.dumps(all_stacks, indent=2))

    slots = [slot_from_stack(stack) for stack in stacks]
    json.dump([slot.toDict() for slot in slots], output, indent=2, sort_keys=True)


def _get_stacks_from_job(job):
    """
    Get the list of stack definitions from the artifacts of the gitlab-ci job
    'generate_stacks'.
    """
    import requests

    try:
        return requests.get(job.web_url + "/artifacts/raw/to_release.json").json()
    except requests.exceptions.RequestException as e:
        click.echo(f"Error fetching stacks from job {job.name}: {e}")
        return []


def get_requested_stacks(repo_url):
    """
    Return the list of stack definitions from merge requests in repo_url.
    """
    # Code copied from https://gitlab.cern.ch/lhcb-core/lb-nightly-builds-frontend/-/blob/master/lb/nightly/frontend/slots_config.py#L304
    from itertools import chain
    from urllib.parse import urlparse

    from gitlab import Gitlab

    url = urlparse(repo_url)
    project_id = url.path.lstrip("/")
    gitlab_url = f"{url.scheme}://{url.netloc}"

    gitlab = Gitlab(gitlab_url, private_token=os.environ.get("GITLAB_TOKEN"))
    project = gitlab.projects.get(project_id)

    # build a dictionaries of stack names to make sure that we have only
    # one definition per name (if )
    stacks = {
        stack["name"]: (stack, mr.iid)
        for stack, mr in chain.from_iterable(
            [(stack, mr) for stack in _get_stacks_from_job(job)]
            # for each ready merge request
            for mr in project.mergerequests.list(state="opened", wip="no", all=True)
            # take the latest successful pipeline (they come from latest to first)
            for pipeline in [pl for pl in mr.pipelines() if pl["status"] == "success"][
                :1
            ]
            # find the "generate_stacks" job
            for job in project.pipelines.get(pipeline["id"]).jobs.list(all=True)
            if job.name == "generate_stacks"
        )
    }
    for stack, mr_id in stacks.values():
        stack["merge_request"] = mr_id
    return [stack for stack, _ in stacks.values()]


def slot_from_stack(stack):
    """
    Create a Slot instance from a stack definition.
    """
    # Code copied from https://gitlab.cern.ch/lhcb-core/lb-nightly-builds-frontend/-/blob/master/lb/nightly/frontend/slots_config.py#L340
    desc = f"release build for stack {stack['name']}"
    if "merge_request" in stack:
        desc += (
            f' (from <a href="https://gitlab.cern.ch/lhcb-core/lhcbstacks/-/merge_requests/{stack["merge_request"]}"'
            f' target="_blank">lhcb-core/lhcbstacks!{stack["merge_request"]}</a>)'
        )
    build_tool = stack["build_tool"].lower()
    slot = Slot(
        "lhcb-release",
        desc=desc,
        projects=[
            Project(
                name,
                version,  # FIXME check if the tag exists
                checkout_opts={"export": True},
                disabled=(name in stack["deployed"]),
                with_shared=(name == "Geant4"),
            )
            for name, version in stack["projects"].items()
        ],
        platforms=stack["platforms"],
        build_tool=build_tool,
        cache_entries={
            # Special settings required for new style CMake projects
            "GAUDI_LEGACY_CMAKE_SUPPORT": True,
        },
        no_patch=True,
        with_version_dir=True,
        metadata={"lhcbstacks": stack, "for_release": True},
    )
    if build_tool == "cmt" and any("slc5" in p for p in stack["platforms"]):
        # FIXME we should set it only for SLC5 platforms,
        #       but we never mix SLC5 and SLC6 in the same stack
        slot.env.append("CMTEXTRATAGS=host-slc5")
    return slot


# FIXME this is only needed to comply to the API of lbpr-get-command from LbNightlyTools
@config.command()
@click.argument("task", type=TaskId.from_str)
@click.option(
    "-o",
    "--output",
    default="-",
    type=click.File("w"),
    help="Where to write the output to (default to stdout)",
)
def get_json(task: TaskId, output):
    """
    Get the configuration JSONI object of the given slot build.
    """
    # FIXME deduplicate
    d = Dashboard(flavour=task.flavour)
    doc_id = task.doc_id()
    if doc_id not in d.db:
        exit(f"unknown build {task}")

    doc = d[doc_id]
    cfg = doc["config"]
    # FIXME lbpr-get-command requires the field "completed" in cfg
    #       (which used to be set at the end of the checkout)
    if "completed" not in cfg and "completed" in doc.get("checkout", {}):
        cfg["completed"] = doc["checkout"]["completed"]

    json.dump(cfg, output, indent=2, sort_keys=True)


@config.command()
@click.argument("task", type=TaskId.from_str)
@click.argument("platform")
@click.option(
    "--start-timestamp",
    type=int,
    help="Timestamp of the first iteration of the check loop, "
    "overrides the content of the DB",
)
@click.option(
    "--timeout",
    type=int,
    default=0,
    help="Timeout in seconds to override what defined in the configuration",
)
def check_preconditions(
    task: TaskId, start_timestamp: Optional[int], platform: str, timeout: int
):
    """
    Check the preconditions of the given slot build.

    Updates the DB and returns one of the following values:
    - ok
    - not-ok
    - timeout

    For example:
    ```
    task=flavour/slot-name/123
    platform=some-platform
    while true ; do
        case $(lb-ci config check-preconditions ${task} ${platform}) in
          ok) break ;; # all good
          not-ok) ;; # wait more
          timeout) exit 1 ;; # enough waiting
        esac
        sleep 60
    done
    ```
    """
    # FIXME deduplicate
    d = Dashboard(flavour=task.flavour)
    doc_id = task.doc_id()
    if doc_id not in d.db:
        exit(f"unknown build {task}")

    doc = d[doc_id]
    cfg = doc["config"]
    if (
        not cfg.get("preconditions")
        or doc.get("preconditions", {}).get(platform, {}).get("status") == "ok"
    ):
        # no precondition to check of we already found it good
        click.echo("ok")
        return

    if start_timestamp:
        start_time = datetime.fromtimestamp(start_timestamp)
    elif started := doc.get("preconditions", {}).get(platform, {}).get("started"):
        start_time = datetime.fromisoformat(started)
    else:
        start_time = datetime.now()

    # FIXME in principle we should check the timeout for each prerequisite, but there's
    #       a bug in LbNightlyTools and we cannot express a timeout in the JSON dump of
    #       the config
    # Note: we use a different default timeout wrt LbNightlyTools (7h instead of 17h)
    when_to_stop = start_time + (timedelta(seconds=timeout) or timedelta(hours=7))
    too_late = datetime.now() > when_to_stop

    results = []
    # note that we perform one last check before reporting that we are too late
    # partially because the precondition might have been met in the meantime and
    # partially because we may get a printout of why the precondition is not met
    for precondition in cfg["preconditions"]:
        # the precondition functions in LbNightlyTools are meant to wait
        # but this command should return immediately, so we need a few hacks
        checker = getattr(Preconditions, precondition["name"])
        args = precondition["args"]
        args["timeout"] = timedelta(seconds=5)
        # the checker are enforcing a sleep of 60 seconds if the precondition
        # is not matched so we need to patch it to be able to return immediately
        with patch("LbNightlyTools.Scripts.Preconditions.sleep"), temp_env(
            {"BINARY_TAG": platform}
        ):
            results.append(checker(**args))

    status = "ok" if all(results) else "not-ok" if not too_late else "timeout"

    def update_doc(doc):
        preconditions = doc.setdefault("preconditions", {})
        preconditions[platform] = {
            "started": start_time.isoformat(),
            "status": status,
            "last_check": datetime.now().isoformat(),
            "host": gethostname(),
            "build_url": os.environ.get("BUILD_URL", ""),
        }

    update_db(d.db, doc_id, update_doc)

    click.echo(status)
