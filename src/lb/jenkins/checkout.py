import contextlib
import json
import logging
import os
import zipfile
from datetime import datetime
from io import BytesIO
from multiprocessing import Pool
from pathlib import Path
from shutil import rmtree
from subprocess import run
from tempfile import NamedTemporaryFile
from typing import Mapping, Optional

import click
import LbNightlyTools.Utils
from LbNightlyTools.Configuration import DataProject, Package, Slot

from .ci import lb_ci
from .common import TaskId, lbenv, step_info_initializer, temp_env, update_db
from .repository import connect as connect_repo
from .repository import decode_artifacts_config

logger = logging.getLogger(__name__)


class CheckoutSubtask:
    __slots__ = (
        "project",
        "workspace",
        "artifacts_config",
        "artifacts_slot_prefix",
        "doc",
    )

    def __init__(
        self,
        doc: Mapping,
        project: str | list[str],
        workspace: Path,
        artifacts_config: Optional[Mapping],
        artifacts_slot_prefix: Path,
    ):
        self.doc = doc
        self.project = project if isinstance(project, list) else [project]
        self.workspace = workspace
        self.artifacts_config = artifacts_config
        self.artifacts_slot_prefix = artifacts_slot_prefix


@lb_ci.command()
@click.argument("task", type=TaskId.from_str)
@click.option(
    "-j",
    "--processes",
    type=click.IntRange(min=1),
    help="Number of parallel processes to use (default to the number of CPUs)",
)
@click.option(
    "-a",
    "--artifacts-repo",
    metavar="ARTIFACTS_REPO_CONFIG",
    type=decode_artifacts_config,
    default="",
    help="Where to store checkout artifacts",
)
@click.option(
    "-w",
    "--workspace",
    type=click.Path(dir_okay=True, file_okay=False),
    default="workspace",
    help="Where to prepare the workspace layout",
)
def checkout(task: TaskId, processes: int, artifacts_repo: Mapping, workspace: str):
    """
    Checkout all projects declared in a slot, applying the needed merge requests,
    optionally publishing the archives with the code to an artifacts repository.
    """
    # FIXME deduplicate
    d = LbNightlyTools.Utils.Dashboard(flavour=task.flavour)
    doc_id = task.doc_id()
    if doc_id not in d.db:
        exit(f"unknown build {task}")

    doc = update_db(d.db, doc_id, step_info_initializer(["checkout"], projects={}))

    slot = Slot.fromDict(doc["config"])

    logger.debug("checkout projects for %s in %s", task, workspace)

    # FIXME deduplicate
    artifacts_slot_prefix = Path(str(task)) / "packs" / "src"
    workspace_path = Path(workspace).resolve()
    workspace_path.mkdir(parents=True, exist_ok=True)

    repo = connect_repo(artifacts_repo) if artifacts_repo else None

    # FIXME: add handling of data packages

    pool = Pool(processes)
    # it would be nice to have a Python version of https://doc.rust-lang.org/std/iter/trait.Iterator.html#method.partition
    data_projects = []
    projects = []
    for project in slot.activeProjects:
        (data_projects if isinstance(project, DataProject) else projects).append(
            project
        )
    names = [project.name for project in projects]
    names.extend([dp.name, pkg.name] for dp in data_projects for pkg in dp.packages)

    # this will give us visual feedback of the checkout starting
    # although it's a bit fake because it shows all projects as
    # started at the same time, but it's better than nothing and
    # later we will record the actual start time
    # FIXME improve the granularity of db updates
    def mark_projects_as_started(doc):
        doc["checkout"]["projects"] = {
            (name if isinstance(name, str) else name[-1]): {
                "started": doc["checkout"]["started"]
            }
            for name in names
        }

    doc = update_db(d.db, doc_id, mark_projects_as_started)

    results = pool.imap_unordered(
        checkout_project,
        [
            CheckoutSubtask(
                doc, name, workspace_path, artifacts_repo, artifacts_slot_prefix
            )
            for name in names
        ],
    )

    logs = {}

    for output in results:
        project = output.pop("project")
        if not output:
            logger.info("no output for %s", project)
            continue
        logs[project] = output.pop("log")
        del output["stdout"]
        del output["stderr"]
        update_db(
            d.db,
            doc_id,
            lambda doc: doc["checkout"]["projects"].update({project: output}),
        )
        click.echo(f"{project} -> {output}")

    # publish checkout logs as a zip file
    if repo:
        pack = BytesIO()
        with zipfile.ZipFile(pack, "w", zipfile.ZIP_DEFLATED) as zf:
            # FIXME to be removes once we deploy lhcb-core/LbNightlyTools!433
            zf.writestr(
                "dummy",
                "see https://gitlab.cern.ch/lhcb-core/LbNightlyTools/-/merge_requests/433",
            )
            for project, log in logs.items():
                zf.writestr(Path("checkout").joinpath(f"{project}.log").as_posix(), log)

        pack.seek(0)
        if not repo.push(pack, f"{task}/checkout.zip"):
            raise RuntimeError(
                f"failed to publish checkout.zip to {artifacts_repo['uri']}"
            )

        # FIXME this is needed by lbn-install, but the info is in CouchDB already
        repo.push(
            BytesIO(json.dumps(doc["config"], indent=2).encode()),
            f"{task}/slot-config.json",
        )

    publish_live_unpacking_helpers(repo, task)

    update_db(
        d.db,
        doc_id,
        lambda doc: doc["checkout"].update({"completed": datetime.now().isoformat()}),
    )


def checkout_project(task: CheckoutSubtask):
    # FIXME add re-use of checkout artifacts

    slot = Slot.fromDict(task.doc["config"])
    project = slot
    for name in task.project:
        project = getattr(project, name)
    artifact = task.artifacts_slot_prefix / f"{project.name}.tar.zst"

    # FIXME we should have an option to prevent LbNightlyTools checkout
    # to post messages on Gitlab
    # if "GITLAB_TOKEN" in os.environ:
    #     del os.environ["GITLAB_TOKEN"]

    output = {"project": project.name}
    if isinstance(project, DataProject):
        return output
    with contextlib.chdir(task.workspace):
        # make sure git does not get anything in its way
        # (we have to take into account the deployment-like layout too)
        for name in (project.name, project.name.upper()):
            if os.path.isdir(name):
                rmtree(name)
            elif os.path.islink(name) or os.path.exists(name):
                os.remove(name)
        # do checkout
        output["started"] = datetime.now().isoformat()
        output.update(project.checkout())
        if (
            hasattr(project, "patch") and not project.slot.no_patch
        ):  # Packages do not have the patch method
            with (Path(project.baseDir) / f"{project.name}.patch").open(
                "w"
            ) as patchfile:
                # FIXME is this the right way to patch?
                project.patch(patchfile)
        if isinstance(project, Package):  # data packages require a bit more
            if not hasattr(project, "checkout_log"):
                project.checkout_log = ""
            # for data package builds we need an LbEnv environment,
            # plus CMTCONFIG even if we do not need it otherwise cmt does not work
            # (because of a bug in Package.build, we cannot pass the kwarg "env"
            # to build)
            env = lbenv()
            env["CMTCONFIG"] = "Linux-x86_64"
            with temp_env(env):
                project.build()
            if hasattr(project, "build_log"):
                project.checkout_log += project.build_log
            for link in project.getVersionLinks():
                dest = Path(project.baseDir).parent / link
                project.checkout_log += (
                    f"creating symlink {link} for {project.name}/{project.version}\n"
                )
                os.symlink(project.version, dest)

        output["completed"] = datetime.now().isoformat()
        # record extra info
        if hasattr(project, "checkout_log"):
            output["log"] = project.checkout_log
        output["artifact"] = str(artifact)
        # FIXME this is because of a bug in LbNightlyTools checkout:
        # it wants to looks for MRs for HEAD if there are none (although
        # we already looked for them)
        try:
            output["warning"].remove("merge requests not applied: no Gitlab token")
        except (KeyError, ValueError):
            pass
        # keep a copy of the report
        Path(project.baseDir).mkdir(parents=True, exist_ok=True)
        with open(Path(project.baseDir) / ".checkout.json", "w") as report_file:
            json.dump(output, report_file)

        # publish the artifact
        if task.artifacts_config:
            repo = connect_repo(task.artifacts_config)
            # FIXME deduplicate
            # FIXME we can use TemporaryFile and pipe the stdout of tar
            with NamedTemporaryFile(suffix=".tar.zst") as tmp_file:
                run(
                    [
                        "tar",
                        "--create",
                        "--auto-compress",
                        f"--file={tmp_file.name}",
                        project.baseDir,
                    ],
                    check=True,
                )
                if not repo.push(tmp_file, output["artifact"]):
                    raise RuntimeError(
                        "failed to publish "
                        f"{artifact} to {task.artifacts_config['uri']}"
                    )

    return output


def publish_live_unpacking_helpers(repo, task):
    from LbNightlyTools.Scripts.Checkout import HTACCESS_DATA
    from LbNightlyTools.Scripts.Checkout import __file__ as module_path

    if not repo:
        return

    repo.push(BytesIO(HTACCESS_DATA.encode()), f"{task}/.htaccess")
    for helper in ("extract.php", "listzip.php"):
        with (Path(module_path).parent / helper).open("rb") as f:
            repo.push(f, f"{task}/{helper}")
