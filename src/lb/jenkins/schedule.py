import logging
from datetime import datetime
from pathlib import Path
from time import sleep

import click
import couchdb
import LbNightlyTools.Utils
import networkx as nx
from LbNightlyTools.Configuration import DataProject, Slot
from LbPlatformUtils import requires

from .ci import lb_ci
from .common import TaskId, is_slot_aborted

logger = logging.getLogger(__name__)


@lb_ci.command()
@click.argument("task", type=TaskId.from_str)
@click.option(
    "--next-tasks-dir",
    type=click.Path(dir_okay=True, file_okay=False),
    default=".",
    help="where the next jobs property files have to be created",
)
def schedule(task: TaskId, next_tasks_dir: str):
    """
    Compute what to do next for a given task id in the form
    `<flavour>/<slot>/<build_id>`.

    The output of the script is (optionally) a number of Jenkins job property
    files in the directory specified by --next-tasks-dir
    """
    logger.debug("scheduling %s", task)
    # FIXME deduplicate
    d = LbNightlyTools.Utils.Dashboard(flavour=task.flavour)
    try:
        doc = d[task.doc_id()]
    except couchdb.ResourceNotFound:
        exit(f"unknown build {task}")

    next_tasks_path = Path(next_tasks_dir)

    # A slot build may need to run the checkout or the builds.
    # If the checkout is not complete we cannot start the builds
    # as the dependencies information might be incomplete.
    if is_slot_aborted(doc):
        # but, of course, nothing to do on aborted slots
        click.echo(f"{task} aborted, there's nothing else to do")
    elif is_checkout_running(doc):
        click.echo(f"{task} checkout is running")
    elif not is_checkout_done(doc):
        logger.info("%s needs checkout", task)
        next_tasks_path.mkdir(parents=True, exist_ok=True)
        with open(next_tasks_path / "checkout.txt", "w") as f:
            f.write(f"task={task}\n")
        # we can assume there is only one scheduler job running if
        # checkout was not started yet, so we do not need to handle
        # update conflicts
        doc["checkout"] = {"scheduled": datetime.now().isoformat()}
        d.db.save(doc)

        # while checking out, we can start checking for preconditions
        if platforms := preconditions_to_check(doc):
            logger.info("%s needs to check for preconditions", task)
            next_tasks_path.mkdir(parents=True, exist_ok=True)
            for n, platform in enumerate(platforms):
                with open(next_tasks_path / f"preconditions-{n}.txt", "w") as f:
                    f.write(f"task={task}\n")
                    f.write(f"platform={platform}\n")
            # same as for the checkout, no need to handle conflicts
            start_time = datetime.now().isoformat()
            doc["preconditions"] = {
                platform: {"started": start_time} for platform in platforms
            }
            d.db.save(doc)

    elif to_build := builds_pending(doc):
        logger.info("%s needs builds", task)
        next_tasks_path.mkdir(parents=True, exist_ok=True)
        for n, (project, platform) in enumerate(to_build):
            required_microarch = requires(platform).split("-")[0]
            label_expression = f"build && {required_microarch}"
            # we expect slot metadata.node_overrides to possibly have
            # a key like "project/platform" with the label expression to use
            # with "<default>" as a placeholder for the deduced label expression
            if (
                override := doc["config"]
                .get("metadata", {})
                .get("node_overrides", {})
                .get(f"{project}/{platform}")
            ):
                label_expression = override.replace("<default>", label_expression)
            with open(next_tasks_path / f"build-{n}.txt", "w") as f:
                f.write(
                    f"task={task}\n"
                    f"node={label_expression}\n"
                    f"project={project}/{platform}\n"
                )
        retry = 5
        while retry:
            try:
                doc = d[task.doc_id()]
                now = datetime.now().isoformat()
                for project, platform in to_build:
                    if "builds" not in doc:
                        doc["builds"] = {}
                    if platform not in doc["builds"]:
                        doc["builds"][platform] = {}
                    if project not in doc["builds"][platform]:
                        doc["builds"][platform][project] = {}
                    doc["builds"][platform][project]["scheduled"] = now
                d.db.save(doc)
                break
            except couchdb.ResourceConflict:  # pragma: no cover
                # FIXME we should test that the retry works
                logger.warning("update conflict, retrying")
                retry -= 1
                sleep(1)
        else:  # pragma: no cover
            exit(f"failed to update {task} in the database")
    else:
        click.echo(f"nothing to do for {task}")


def is_checkout_done(doc) -> bool:
    """
    Return True if the checkout is flagged as completed and successful in the document.
    """
    # the document is expected to have a key "checkout" pointing to a dictionary with
    # "scheduled", "started" or "completed" entries. We assume that if "completed" is
    # there we do not need a checkout anymore
    return bool(doc.get("checkout", {}).get("completed"))


def is_checkout_running(doc) -> bool:
    """
    Return True if the checkout is flagged as completed and successful in the document.
    """
    # the document is expected to have a key "checkout" pointing to a dictionary with
    # "scheduled", "started" or "completed" entries. The checkout is running if either
    # "scheduled" or "started" is there and it's not completed.
    return bool(
        doc.get("checkout", {}).get("scheduled")
        or doc.get("checkout", {}).get("started")
    ) and not is_checkout_done(doc)


def preconditions_to_check(doc) -> list[str]:
    """
    Return the list of platforms that need preconditions to be checked.
    """
    if not doc.get("config", {}).get("preconditions"):
        return []
    return [
        platform
        for platform in doc["config"]["platforms"]
        # if there's something in the preconditions for this platform
        # it's being checked or already checked
        if not doc.get("preconditions", {}).get(platform)
    ]


def builds_pending(doc) -> list[tuple[str, str]]:
    """
    Return a list of builds that are pending.
    """
    slot = Slot.fromDict(doc["config"])

    def not_to_build(project_name, _platform):
        project = getattr(slot, project_name)
        return (
            isinstance(project, DataProject)
            or not project.enabled
            or project.platform_independent
            or str(project.build_tool) == "no-build"
        )

    def is_deployed(project_name, platform):
        # FIXME we should add a mechanism to update the status
        #       and check it's not "FAILURE"
        return bool(
            doc.get("deployment", {})
            .get(f"{project_name}/{platform}", {})
            .get("status")
        )

    def is_scheduled_or_running(project_name, platform):
        project = getattr(slot, project_name)
        # when we schedule a build task we flag it as scheduled
        # FIXME report a problem if a task is scheduled for too long
        state = doc.get("builds", {}).get(platform, {}).get(project.name, {})
        return bool(state.get("scheduled") or state.get("started"))

    # if preconditions are needed ignore platforms that are not OK
    if slot.preconditions:
        platforms = [
            platform
            for platform in slot.platforms
            if doc.get("preconditions", {}).get(platform, {}).get("status") == "ok"
        ]
    else:
        platforms = slot.platforms

    # we build a graph from dependee to dependant (e.g. Gaudi -> LHCb)
    build_tasks = nx.DiGraph()
    build_tasks.add_nodes_from(
        (project.name, platform)
        for project in slot.activeProjects
        for platform in platforms
    )
    dependencies = slot.dependencies()
    for project, platform in list(build_tasks.nodes):
        build_tasks.add_edges_from(
            ((dep, platform), (project, platform)) for dep in dependencies[project]
        )
    # we drop the projects that are already deployed (or do not need to be built)
    build_tasks.remove_nodes_from(
        list(
            (project, platform)
            for project, platform in build_tasks.nodes
            if not_to_build(project, platform) or is_deployed(project, platform)
        )
    )
    # then return all (project, platform) pairs with no dependency (i.e. in-degree 0)
    # and that are not already scheduled
    return [
        node
        for node, in_degree in build_tasks.in_degree()
        if in_degree == 0 and not is_scheduled_or_running(*node)
    ]


@lb_ci.command()
@click.argument("task", type=TaskId.from_str)
@click.argument("project_platform")
@click.option(
    "--next-tasks-dir",
    type=click.Path(dir_okay=True, file_okay=False),
    default=".",
    help="where the next jobs property files have to be created",
)
def schedule_pr_tests(task: TaskId, project_platform: str, next_tasks_dir: str):
    """
    Get the list of LHCb PR tests to run for a given slot, project, platform.

    The output is (optionally) a number of Jenkins job property files
    in the directory specified by --next-tasks-dir
    """
    from textwrap import dedent

    from lbpr_fetch_tests import fetch_tests

    # FIXME deduplicate
    d = LbNightlyTools.Utils.Dashboard(flavour=task.flavour)
    try:
        doc = d[task.doc_id()]
    except couchdb.ResourceNotFound:
        exit(f"unknown build {task}")

    # FIXME should we handle the case of aborted slots?
    next_tasks_path = Path(next_tasks_dir)

    project_name, platform = project_platform.split("/")
    labels = doc["config"].get("metadata", {}).get("labels", [])

    for n, test in enumerate(
        fetch_tests(
            slot_name=task.slot,
            project_name=project_name,
            platform=platform,
            labels=labels,
        )
    ):
        with open(next_tasks_path / f"lhcb-pr-test-{n}.txt", "w") as f:
            f.write(
                dedent(f"""\
                    task={task}
                    project={project_platform}
                    node={test.jenkins_label}
                    name={test.name}
                    exec_name={test.exec_name}
                    handlers={test.handlers}
                    runs_count={test.runs_count}
                    """)
            )
