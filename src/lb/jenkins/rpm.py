import asyncio
import logging
import os
import shutil
from multiprocessing import Pool
from pathlib import Path
from subprocess import run
from tempfile import TemporaryDirectory

import click
import LbNightlyTools.Utils
from LbNightlyTools.Configuration import Project, Slot

from .build import unpack
from .ci import lb_ci
from .common import TaskId
from .repository import connect as connect_repo
from .repository import decode_artifacts_config


@lb_ci.group()
def rpm():
    """
    Commands to work with RPMs.
    """
    pass


@rpm.command()
@click.argument("task", type=TaskId.from_str)
@click.option(
    "-j",
    "--processes",
    type=click.IntRange(min=1),
    help="Number of parallel processes to use (default to the number of CPUs)",
)
@click.option(
    "-a",
    "--artifacts-repo",
    metavar="ARTIFACTS_REPO_CONFIG",
    type=decode_artifacts_config,
    default="",
    help="Where to store checkout artifacts",
)
@click.option(
    "-w",
    "--workspace",
    type=click.Path(dir_okay=True, file_okay=False),
    default="workspace",
    help="Where to prepare the workspace layout",
)
@click.option(
    "--force",
    is_flag=True,
    default=False,
    help="Generate the source RPM even for non-release slots",
)
def make_src(
    task: TaskId, processes: int, artifacts_repo: dict, workspace: str, force: bool
):
    """
    Generate and publish RPMs for the projects source files in a release slot.

    Nothing is done if the slot is not a release slot (unless --force is used).
    """
    # FIXME deduplicate
    d = LbNightlyTools.Utils.Dashboard(flavour=task.flavour)
    doc_id = task.doc_id()
    if doc_id not in d.db:
        exit(f"unknown build {task}")

    doc = d[doc_id]
    if not force and not doc["config"].get("metadata", {}).get("for_release"):
        click.echo(f"{task} is not a release build, nothing to do")
        return

    slot = Slot.fromDict(doc["config"])

    with TemporaryDirectory() as tmpdir:
        artifacts_dir = Path(tmpdir)
        (artifacts_dir / "packs" / "src").mkdir(parents=True, exist_ok=True)
        pool = Pool(processes=processes)
        pool.starmap(
            _process_src_rpm,
            [
                (
                    task,
                    doc["config"],
                    project.name,
                    artifacts_dir,
                    workspace,
                    artifacts_repo,
                )
                for project in slot.activeProjects
            ],
        )
        pool.close()
        pool.join()


@rpm.command()
@click.argument("task", type=TaskId.from_str)
@click.argument("project_platform")
@click.option(
    "-a",
    "--artifacts-repo",
    metavar="ARTIFACTS_REPO_CONFIG",
    type=decode_artifacts_config,
    default="",
    help="Where the checkout artifacts are stored and the build "
    "artifacts are published",
)
@click.option(
    "-w",
    "--workspace",
    type=click.Path(dir_okay=True, file_okay=False),
    default="workspace",
    help="Where to prepare the workspace layout",
)
@click.option(
    "--force",
    is_flag=True,
    default=False,
    help="Generate the binary RPM even for non-release slots",
)
def make_bin(
    task: TaskId,
    project_platform: str,
    artifacts_repo: dict,
    workspace: str,
    force: bool,
):
    """
    Generate and publish RPMs for the projects binary files in a release slot.

    Nothing is done if the slot is not a release slot (unless --force is used).
    """
    # FIXME deduplicate
    d = LbNightlyTools.Utils.Dashboard(flavour=task.flavour)
    doc_id = task.doc_id()
    if doc_id not in d.db:
        exit(f"unknown build {task}")

    doc = d[doc_id]
    if not force and not doc["config"].get("metadata", {}).get("for_release"):
        click.echo(f"{task} is not a release build, nothing to do")
        return

    project_name, platform = project_platform.split("/")

    with TemporaryDirectory() as tmpdir:
        artifacts_dir = Path(tmpdir)
        _process_bin_rpm(
            task,
            doc["config"],
            project_name,
            platform,
            artifacts_dir,
            workspace,
            artifacts_repo,
        )


@rpm.command()
@click.argument("task", type=TaskId.from_str)
@click.argument("project_platform")
@click.option(
    "-a",
    "--artifacts-repo",
    metavar="ARTIFACTS_REPO_CONFIG",
    type=decode_artifacts_config,
    default="",
    help="Where the checkout artifacts are stored and the build "
    "artifacts are published",
)
@click.option(
    "-w",
    "--workspace",
    type=click.Path(dir_okay=True, file_okay=False),
    default="workspace",
    help="Where to prepare the workspace layout",
)
@click.option(
    "--force",
    is_flag=True,
    default=False,
    help="Generate the index RPM even for non-release slots or " "non-first platforms",
)
def make_index(
    task: TaskId,
    project_platform: str,
    artifacts_repo: dict,
    workspace: str,
    force: bool,
):
    """
    Generate and publish RPMs for the glimpse index of projects a release slot.

    Nothing is done if the slot is not a release slot or the platform is not
    the first of those listed in the configuration. The index requires binary
    artifacts, but it does not matter which platform is used as long as it
    is produced exactly once. The option --force can be used to generate the
    index in any case, for debugging purposes.
    """
    # FIXME deduplicate
    d = LbNightlyTools.Utils.Dashboard(flavour=task.flavour)
    doc_id = task.doc_id()
    if doc_id not in d.db:
        exit(f"unknown build {task}")

    project_name, platform = project_platform.split("/")

    doc = d[doc_id]
    if not force:
        if not doc["config"].get("metadata", {}).get("for_release"):
            click.echo(f"{task} is not a release build, nothing to do")
            return
        if platform != doc["config"]["platforms"][0]:
            click.echo(f"{platform} is not the first platform of {task}, nothing to do")
            return

    with TemporaryDirectory() as tmpdir:
        # if possible, use a fresh copy of the workspace
        workspace_dir = (Path(tmpdir) / "build").resolve()
        if artifacts_repo:
            repo = connect_repo(artifacts_repo)
            workspace_dir.mkdir(parents=True, exist_ok=True)
            # Note that the binary artifact includes the sources
            asyncio.run(
                unpack(
                    repo,
                    doc["builds"][platform][project_name]["artifact"],
                    workspace_dir,
                )
            )
        else:
            workspace_dir.symlink_to(Path(workspace).resolve())

        artifacts_dir = Path(tmpdir)
        _process_index_rpm(
            task,
            doc["config"],
            project_name,
            platform,
            artifacts_dir,
            str(workspace_dir),
            artifacts_repo,
            cwd=tmpdir,
        )


def _process_src_rpm(
    task: TaskId,
    slot_doc: dict,
    project_name: str,
    artifacts_dir: Path,
    workspace: str,
    artifacts_repo: dict,
):
    # for each project in the slot we have to
    # - produce the zip file `{artifacts_dir}/packs/src/{project}.{version}.src.zip`
    # - call `lbn-rpm --shared ...`
    # - publish to artifacts repo
    slot, project = _slot_project(slot_doc, project_name)
    archive_path = (
        artifacts_dir / "packs" / "src" / f"{project.name}.{project.version}.src.zip"
    )
    logging.debug("packing sources as %s", archive_path)
    run(
        ["zip", "-r", "-q", "-y", str(archive_path.resolve()), project.baseDir],
        check=True,
        cwd=workspace,
    )
    logging.debug("building %s src RPM", project_name)
    run(
        [
            "lbn-rpm",
            "--projects",
            project_name.lower(),
            "--shared",
            "--build-id",
            "",
            "--artifacts-dir",
            str(artifacts_dir),
            "--builddir",
            workspace,
            "--flavour",
            slot.metadata["flavour"],
            f"{slot.name}.{slot.build_id}",
        ],
        check=True,
    )
    _publish_rpms(
        task,
        artifacts_dir,
        artifacts_repo,
        f"{project.name.upper()}_{project.version}-*.rpm",
    )


def _process_bin_rpm(
    task: TaskId,
    slot_doc: dict,
    project_name: str,
    platform: str,
    artifacts_dir: Path,
    workspace: str,
    artifacts_repo: dict,
):
    # lbn-rpm ${loglevel_opt} ${flavour:+--flavour ${flavour}}
    #  --build-id "${slot}.${slot_build_id}" --artifacts-dir "${directory}"
    #  "${slot}.${slot_build_id}" --platform "${platform}" --checkrelease
    slot, project = _slot_project(slot_doc, project_name)
    logging.debug("building %s %s RPM", project_name, platform)
    run(
        [
            "lbn-rpm",
            "--projects",
            project.name.lower(),
            "--platform",
            platform,
            "--build-id",
            "",
            "--artifacts-dir",
            str(artifacts_dir),
            "--builddir",
            workspace,
            "--flavour",
            slot.metadata["flavour"],
            f"{slot.name}.{slot.build_id}",
        ],
        check=True,
    )
    _publish_rpms(
        task,
        artifacts_dir,
        artifacts_repo,
        f"{project.name.upper()}_{project.version}_{platform.replace('-', '_')}-*.rpm",
    )


def _process_index_rpm(
    task: TaskId,
    slot_doc: dict,
    project_name: str,
    platform: str,
    artifacts_dir: Path,
    workspace: str,
    artifacts_repo: dict,
    cwd: str,
):
    slot, project = _slot_project(slot_doc, project_name)

    cmake_binary_dir = Path(workspace) / project.baseDir / "build"
    cmake_binary_dir_moved = False
    if cmake_binary_dir.exists():
        # lbn-index ignores "build.*" directories, but not "build"
        # which is what we use in the CMake builds
        os.rename(
            cmake_binary_dir,
            cmake_binary_dir.parent / "build.do_not_index",
        )
        cmake_binary_dir_moved = True

    # lbn-index ${loglevel_opt}
    #  ${flavour:+--flavour ${flavour}}
    #  --build-id "${slot}.${slot_build_id}"
    #  --artifacts-dir "${directory}"
    #  "${slot}.${slot_build_id}"
    logging.debug("building %s index", project_name)
    run(
        [
            "lbn-index",
            "--build-id",
            "",
            "--artifacts-dir",
            str(artifacts_dir),
            "--flavour",
            slot.metadata["flavour"],
            f"{slot.name}.{slot.build_id}",
        ],
        cwd=cwd,
        check=True,
    )

    if cmake_binary_dir_moved:
        os.rename(
            cmake_binary_dir.parent / "build.do_not_index",
            cmake_binary_dir,
        )

    # lbn-rpm --glimpse ${loglevel_opt} ${flavour:+--flavour ${flavour}}
    #  --build-id "${slot}.${slot_build_id}" --artifacts-dir
    #  "${directory}" --checkrelease "${slot}.${slot_build_id}"
    logging.debug("building %s index RPM", project_name)
    run(
        [
            "lbn-rpm",
            "--glimpse",
            "--projects",
            project.name.lower(),
            "--platform",
            platform,
            "--build-id",
            "",
            "--artifacts-dir",
            str(artifacts_dir),
            "--flavour",
            slot.metadata["flavour"],
            f"{slot.name}.{slot.build_id}",
        ],
        cwd=cwd,
        check=True,
    )

    _publish_rpms(
        task,
        artifacts_dir,
        artifacts_repo,
        f"{project.name.upper()}_{project.version}_index-*.rpm",
    )


def _slot_project(slot_doc: dict, project_name: str) -> tuple[Slot, Project]:
    logging.debug("processing %s", project_name)
    slot = Slot.fromDict(slot_doc)
    project = getattr(slot, project_name)
    if project.platform_independent:
        # we do not build RPMs for platform independent projects in the CI
        raise ValueError(
            f"platform independent projects are not supported: {project_name}"
        )
    return slot, project


def _publish_rpms(task, artifacts_dir, artifacts_repo, glob_pattern):
    rpms = artifacts_dir.glob(f"rpms/{glob_pattern}")
    if artifacts_repo:
        repo = connect_repo(artifacts_repo)
        for rpm in rpms:
            logging.debug("publishing %s", rpm.name)
            with rpm.open(mode="rb") as f:
                repo.push(f, f"{task}/rpms/{rpm.name}")
    else:
        for rpm in rpms:
            shutil.copy(rpm, ".")
