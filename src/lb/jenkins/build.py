import asyncio
import contextlib
import itertools
import json
import logging
import os
import re
import shutil
import subprocess
import zipfile
from datetime import datetime, timedelta
from pathlib import Path
from socket import gethostname
from subprocess import run
from tempfile import TemporaryDirectory
from typing import Mapping, Optional

import click
import couchdb
import LbNightlyTools.Utils
from LbNightlyTools.Configuration import Project, Slot
from LbNightlyTools.Scripts.Build import genBuildReport
from LbNightlyTools.Scripts.CollectBuildLogs import Script as CBLScript

from . import common
from .ci import lb_ci
from .common import (
    DEPLOYMENT_ROOT,
    TaskId,
    check_deployed_deps,
    is_slot_aborted,
    step_info_initializer,
    temp_env,
    update_db,
    with_krb5_token,
)
from .repository import ArtifactsRepository, decode_artifacts_config
from .repository import connect as connect_repo

logger = logging.getLogger(__name__)


@lb_ci.command()
@click.argument("task", type=TaskId.from_str)
@click.argument("project_platform")
@click.option(
    "-j",
    "--processes",
    type=click.IntRange(min=1),
    help="Number of parallel processes to use (default to the number of CPUs)",
)
@click.option(
    "-a",
    "--artifacts-repo",
    metavar="ARTIFACTS_REPO_CONFIG",
    type=decode_artifacts_config,
    default="",
    help="Where the checkout artifacts are stored and the build "
    "artifacts are published",
)
@click.option(
    "-w",
    "--workspace",
    type=click.Path(dir_okay=True, file_okay=False),
    default="workspace",
    help="Where to prepare the workspace layout",
)
@click.option(
    "--with-krb5",
    metavar="JSON",
    type=json.loads,
    help="Credentials to obtain a Kerberos token as JSON "
    "object with 'principal' and 'keytab' keys",
)
def build(
    task: TaskId,
    project_platform: str,
    processes: Optional[int],
    artifacts_repo: Mapping,
    workspace: str,
    with_krb5: Optional[Mapping],
):
    """
    Build a specific project for a specific platform (passed as 'project/platform')
    as declared in a slot, publishing the archive with the build artifacts
    to an artifacts repository.
    """
    project_name, platform = project_platform.split("/")

    # FIXME deduplicate
    d = LbNightlyTools.Utils.Dashboard(flavour=task.flavour)
    doc_id = task.doc_id()
    try:
        doc = d[doc_id]
    except couchdb.ResourceNotFound:
        exit(f"unknown build {task}")

    if is_slot_aborted(doc):
        click.echo(f"{task} aborted, no need to build")
        return

    doc = update_db(
        d.db,
        doc_id,
        step_info_initializer(
            ["builds", platform, project_name], timestamp_key="preparing"
        ),
    )

    try:
        workspace_path = Path(workspace).resolve()
        workspace_path.mkdir(parents=True, exist_ok=True)

        repo = connect_repo(artifacts_repo)
        assert repo

        asyncio.run(prepare(task, doc, project_name, platform, workspace_path, repo))

        update_db(
            d.db,
            doc_id,
            lambda doc: doc["builds"][platform][project_name].update(
                {"started": datetime.now().isoformat()}
            ),
        )

        env = common.build_env(platform)
        # to make ccache work despite upstream projects being deployed to CVMFS,
        # we have to bind the deployment directory to a reproducible path inside
        # the build container
        # if the directory does not exist it means we do not need it
        # (checked in prepare)
        apptainer_flags = []
        if (DEPLOYMENT_ROOT / str(task)).exists():
            apptainer_flags.append(
                f"--bind '{DEPLOYMENT_ROOT / str(task)}:/dependencies'"
            )

        if "X509_USER_PROXY" in env:
            apptainer_flags.append(
                f"--bind '{env['X509_USER_PROXY']}:{env['X509_USER_PROXY']}'"
            )

        if "APPTAINER_FLAGS" in env:
            apptainer_flags.append(env["APPTAINER_FLAGS"])

        if apptainer_flags:
            env["APPTAINER_FLAGS"] = " ".join(apptainer_flags)

        ccache = Ccache(
            workspace_path / ".ccache", workspace_path, search_path=env["PATH"]
        )

        slot = Slot.fromDict(doc["config"])
        slot.env.extend(
            [
                # ccache configuration
                f"CCACHE_DIR={map_to_container(ccache.path)}",
                f"CCACHE_BASEDIR={map_to_container(ccache.base)}",
                # tell ccache to call the compiler on the files and not on the
                # preprocessed version
                "CCACHE_CPP2=yes",
                "CCACHE_SLOPPINESS=locale",
                "CCACHE_DEPEND=1",
            ]
        )

        project: Project = getattr(slot, project_name)

        build_dir = workspace_path / project.baseDir / "build"

        with contextlib.chdir(workspace_path), temp_env(env, clear=True), (
            with_krb5_token(**with_krb5) if with_krb5 else contextlib.nullcontext()
        ):
            # this is the best place where to record the environment used for the build
            build_env = project.environment()
            result = project.build(
                # We have to explicitly pass the the cache entries
                # (not automatically inherited)
                cache_entries=slot.cache_entries,
                jobs=processes or os.cpu_count(),
                args=["-k"],
                stderr=subprocess.STDOUT,
                # if not in release builds allow missing artifacts
                relaxed_install=not slot.metadata.get("for_release", False),
            )
            # FIXME clarify str or bytes
            if isinstance(result.stdout, str):
                result.stdout = result.stdout.encode("utf-8")

            # FIXME CMT release builds require we generate a manifest.xml by hand
            manifest_file = (
                workspace_path
                / project.baseDir
                / "InstallArea"
                / platform
                / "manifest.xml"
            )
            if (
                str(project.build_tool) == "CMT"
                and slot.metadata.get("for_release", False)
                and result.returncode == 0
                and not manifest_file.exists()
            ):
                from LbNightlyTools.Scripts.Release import createManifestFile

                logger.warning(
                    "%s not generated by the build, we try to produce one",
                    manifest_file,
                )
                manifest_file.parent.mkdir(parents=True, exist_ok=True)
                with temp_env({"CMTPROJECTPATH": f"{DEPLOYMENT_ROOT / str(task)}"}):
                    manifest_file.write_text(
                        createManifestFile(
                            project.name,
                            project.version,
                            platform,
                            workspace_path / project.baseDir,
                        )
                    )

        # FIXME the legacy way to collect build logs has to be replaced
        build_dir.mkdir(parents=True, exist_ok=True)
        with (build_dir / "build-raw.log").open("wb") as log:
            log.write(result.stdout)
        try:
            collect_build_logs(
                build_dir / "build.log",
                build_dir,
                result.stdout,
                build_type=str(project.build_tool),
                platform=platform,
            )
        except RuntimeError as e:
            logging.warning("build collection failed: %s, use fallback", e)
            with (build_dir / "build.log").open("wb") as log:
                # FIXME clarify str or bytes
                log.write(
                    project.build_log.encode("utf-8")
                    if isinstance(project.build_log, str)
                    else project.build_log
                )

        # build job specific infos for the report
        # mask possible secrets in the environment
        for k in build_env:
            if re.search(r"pass(w(or)?d)?|secret|token|admin", k.lower()):
                build_env[k] = "***"

        cpuinfo = LbNightlyTools.Utils.cpuinfo()
        report = genBuildReport(
            str(build_dir / "build.log"),
            str(build_dir / "build_log"),
            str(workspace_path / project.baseDir),
            exceptions={
                "warning": slot.warning_exceptions,
                "error": slot.error_exceptions,
            },
            extra_info={
                "project": project.name,
                "version": project.version,
                "slot": slot.name,
                "slot_build_id": slot.build_id,
                "host": gethostname(),
                "platform": platform,
                "started": result.started.isoformat(),
                "completed": result.completed.isoformat(),
                "retcode": result.returncode,
                "environment": build_env,
                # FIXME we may have different CPUs in a machine
                "cpu": cpuinfo[0] if cpuinfo else {},
                "ccache": {
                    "cmd": ccache.cmd,
                    "version": ccache.version,
                    "stats": ccache.stats(),
                },
            },
        )
        artifact_path = Path(str(task)) / "packs" / platform / f"{project_name}.tar.zst"
        update_db(
            d.db,
            doc_id,
            lambda doc: doc["builds"][platform][project_name].update(
                {
                    "completed": datetime.now().isoformat(),
                    "retcode": result.returncode,
                    "warnings": report["warnings"],
                    "errors": report["errors"],
                    "artifact": str(artifact_path),
                }
            ),
        )

        publish_logs(repo, task, project_name, platform, build_dir)

        publish_artifacts(repo, artifact_path, workspace_path, project.baseDir)

        ccache.evict_older_than(timedelta(days=7))  # prevent ccache from exploding
        publish_artifacts(
            repo,
            Path(
                f"{task.flavour}/{task.slot}/ccache/{platform}/{project_name}.tar.zst"
            ),
            workspace_path,
            ccache.path.name,
        )

    except Exception as e:
        logger.exception(e)
        err = e

        def report_failure(doc):
            report = doc["builds"][platform][project_name]
            report["error"] = str(err)
            report["failed"] = datetime.now().isoformat()

        update_db(
            d.db,
            doc_id,
            report_failure,
        )
        raise


async def prepare(
    task: TaskId,
    doc: Mapping,
    project_name: str,
    platform: str,
    workspace: Path,
    repo: ArtifactsRepository,
):
    slot = Slot.fromDict(doc["config"])
    project: Project = getattr(slot, project_name)

    # asynchronously check if the dependencies are on CVMFS
    dependencies = check_deployed_deps(
        task=task, slot=slot, project=project, platform=platform
    )

    # in the mean time, download and unpack the checkout artifact
    artifact_path = Path(doc["checkout"]["projects"][project_name]["artifact"])

    await asyncio.gather(
        unpack(repo, artifact_path, workspace),
        unpack(
            repo,
            f"{task.flavour}/{task.slot}/ccache/{platform}/{project_name}.tar.zst",
            workspace,
            optional=True,
        ),
    )

    # wait for the dependencies to be deployed
    missing = [project for project, deployed in await dependencies if not deployed]
    if missing:
        missing.sort()
        raise RuntimeError(
            f"Some dependencies are not deployed for {platform}: {missing}"
        )


async def unpack(
    repo: ArtifactsRepository,
    artifact_path: Path | str,
    work_dir: Path,
    optional: bool = False,
):
    try:
        work_dir.mkdir(parents=True, exist_ok=True)
        # wrap repo.pull in a thread and "await" for it to complete
        loop = asyncio.get_running_loop()
        data = await loop.run_in_executor(None, repo.pull, artifact_path)
        # FIXME add support for other compression formats
        cmd = ["tar", "--extract", "--zstd", "--file=-", "--directory", str(work_dir)]
        proc = await asyncio.subprocess.create_subprocess_exec(
            *cmd, stdin=asyncio.subprocess.PIPE
        )
        await proc.communicate(data.getbuffer())
        if not proc.returncode == 0:
            raise RuntimeError(f"Failed to unpack artifact {artifact_path}")
    except (RuntimeError, subprocess.CalledProcessError) as e:
        if not optional:
            raise
        logger.warning(e)


def collect_build_logs(
    output: Path,
    build_dir: Path,
    stdout: Optional[bytes],
    build_type: str,
    platform: str,
):
    build_type = build_type.lower()
    if build_type == "cmake":
        collect_retcode = collect_build_logs_cmake(
            output=output, build_dir=build_dir, stdout=stdout
        )
    elif build_type == "cmt":
        collect_retcode = collect_build_logs_cmt(
            output=output, build_dir=build_dir, stdout=stdout, platform=platform
        )
    else:
        raise ValueError(f"build_type {build_type} is not supported")
    if collect_retcode != 0 or not output.exists():
        raise RuntimeError(f"Failed to collect build logs: {collect_retcode}")


def collect_build_logs_cmake(output: Path, build_dir: Path, stdout: Optional[bytes]):
    special_regions = {}
    loglines = []
    if stdout is not None:
        loglines = stdout.splitlines(True)
        starts = [
            (line.split()[-2], idx)
            for idx, line in enumerate(loglines)
            if line.startswith(b"#### CMake ")
        ]
        end = len(loglines)
        for key, start in starts[-1::-1]:
            special_regions[key] = (start, end)
            end = start
    collect_retcode = CBLScript().run(
        [
            "--debug",
            "--exclude",
            ".*unsafe-install.*",
            "--exclude",
            ".*python.zip.*",
            "--exclude",
            ".*precompile-.*",
            str(build_dir),
            str(output),
        ]
    )

    with output.open("ab") as log:
        for key in [b"unsafe-install", b"post-install", b"install"]:
            start, end = special_regions.get(key, (0, 0))
            log.writelines(loglines[start:end])

    return collect_retcode


def collect_build_logs_cmt(
    output: Path, build_dir: Path, stdout: Optional[bytes], platform: str
):
    return CBLScript().run(
        [
            "--debug",
            "--cmt",
            "--platform",
            platform,
            str(build_dir.parent),
            str(output),
        ]
    )


def publish_logs(
    repo: ArtifactsRepository,
    task: TaskId,
    project_name: str,
    platform: str,
    build_dir: Path,
):
    # prepare build log pack
    # FIXME use TemporaryFile instead of TemporaryDirectory
    with TemporaryDirectory() as tmpdir:
        tmppath = Path(tmpdir)
        with (tmppath / f"{project_name}.zip").open("wb") as pack:
            with zipfile.ZipFile(pack, "w", zipfile.ZIP_DEFLATED) as zf:
                for path in itertools.chain(
                    build_dir.glob("build*.log"), build_dir.glob("build_log/**/*")
                ):
                    zf.write(
                        path.as_posix(),
                        (Path(project_name) / path.relative_to(build_dir)).as_posix(),
                    )
        repo.push(
            (tmppath / f"{project_name}.zip").open("rb"),
            f"{task}/build/{platform}/{project_name}.zip",
        )


def publish_artifacts(
    repo: ArtifactsRepository, artifact_path: Path, root_path: Path, project_name: str
):
    # FIXME deduplicate
    # FIXME we can use TemporaryFile and pipe the stdout of tar
    with TemporaryDirectory() as tmp_dir:
        tmp_file = Path(tmp_dir) / artifact_path.name
        run(
            [
                "tar",
                "--create",
                "--auto-compress",
                f"--file={tmp_file}",
                project_name,
            ],
            check=True,
            cwd=root_path,
        )
        if not repo.push(tmp_file.open("rb"), artifact_path):
            raise RuntimeError(f"failed to publish {artifact_path}")


def map_to_container(path: Path) -> Path:
    """
    Use the same logic of LbNightlyTools.BuildMethods to map
    a host path to a container path.
    """
    # FIXME we should stop mapping the container directory to /workspace
    host_root = Path(os.environ.get("WORKSPACE", ".")).resolve()
    cont_root = Path("/workspace")
    return cont_root / path.relative_to(host_root)


class Ccache:
    INIT_MAX_SIZE = 10  # GiB
    INIT_MAX_FILES = 5000

    def __init__(self, path: Path, base: Path, search_path=None):
        self.path = path
        self.base = base

        cmd = shutil.which("ccache", path=search_path)
        if cmd is None:
            raise RuntimeError("ccache not found")
        self.cmd = cmd
        self.version = run(
            [self.cmd, "--version"], check=True, capture_output=True, text=True
        ).stdout.splitlines()[0]

        if not self.path.exists():
            self.path.mkdir(parents=True)
            run(
                [
                    self.cmd,
                    "-d",
                    str(self.path),
                    "-M",
                    str(self.INIT_MAX_SIZE),
                    "-F",
                    str(self.INIT_MAX_FILES),
                ]
            )

        self.clear_stat()

    def clear_stat(self):
        run([self.cmd, "-d", str(self.path), "-z"])

    def clear(self):
        run([self.cmd, "-d", str(self.path), "-C"])

    def stats(self) -> str:
        return run(
            [self.cmd, "-d", str(self.path), "-s"], capture_output=True, text=True
        ).stdout

    def evict_older_than(self, age: datetime | timedelta | str):
        if isinstance(age, datetime):
            age = datetime.now() - age
        if isinstance(age, timedelta):
            age = f"{int(age.total_seconds())}s"
        return run([self.cmd, "--evict-older-than", age])
