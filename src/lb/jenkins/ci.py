import logging

import click


@click.group()
@click.option("-v", "--verbose", count=True, help="Increase verbosity")
def lb_ci(verbose):
    level = {
        0: logging.WARNING,
        1: logging.INFO,
        2: logging.DEBUG,
    }.get(verbose, logging.DEBUG)
    # FIXME: `force=True` is needed because LbNightlyTools.MergeRequestBuilds
    #        invokes logging.basicConfig at the top level
    logging.basicConfig(level=level, force=True)
