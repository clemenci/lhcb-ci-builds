import contextlib
import json
import logging
import os
import re
import shutil
import subprocess
import zipfile
from collections import defaultdict
from datetime import datetime
from io import BytesIO
from pathlib import Path
from typing import Mapping, Optional
from xml.etree import ElementTree as ET
from xml.sax.saxutils import escape as escape_for_xml

import click
import couchdb
import LbNightlyTools.Utils
from LbNightlyTools.Configuration import Project, Slot
from LbNightlyTools.Scripts.Test import fixFailureCauses

from . import common
from .ci import lb_ci
from .common import (
    DEPLOYMENT_ROOT,
    TaskId,
    is_slot_aborted,
    step_info_initializer,
    temp_env,
    update_db,
    with_krb5_token,
)
from .repository import ArtifactsRepository, decode_artifacts_config
from .repository import connect as connect_repo

logger = logging.getLogger(__name__)


# status translation from CTest xml to the web interface
# (copied from https://gitlab.cern.ch/lhcb-core/LbDevTools/-/blob/master/LbDevTools/data/cmake/CTestXML2HTML#L143
# as CDash schema <https://public.kitware.com/Wiki/CDash:XML>
# does not say anything about possible values)
CDASH_STATUS_MAP = {
    "passed": "PASS",
    "failed": "FAIL",
    "skipped": "SKIPPED",
    "notrun": "SKIPPED",
    "error": "ERROR",
    "untested": "UNTESTED",
}


CTEST_FAILURE_REPORT_TEMPLATE = """\
<?xml version="1.0" encoding="UTF-8"?>
<Site>
	<Testing>
		<StartTestTime>{start_time}</StartTestTime>
        <TestList>
			<Test>ctest</Test>
        </TestList>
        <Test Status="failed">
			<Name>ctest</Name>
			<FullCommandLine></FullCommandLine>
			<Results>
                <NamedMeasurement type="text/string" name="Exit Code">
                    <Value>Failed</Value>
                </NamedMeasurement>
				<NamedMeasurement type="text/string" name="job URL">
					<Value>{job_url}</Value>
				</NamedMeasurement>
				<Measurement>
					<Value>{stdout}</Value>
				</Measurement>
			</Results>
		</Test>
		<EndTestTime>{end_time}</EndTestTime>
    </Testing>
</Site>
"""


@lb_ci.command()
@click.argument("task", type=TaskId.from_str)
@click.argument("project_platform")
@click.option(
    "-j",
    "--processes",
    type=click.IntRange(min=1),
    help="Number of parallel processes to use (default to the number of CPUs)",
)
@click.option(
    "-a",
    "--artifacts-repo",
    metavar="ARTIFACTS_REPO_CONFIG",
    type=decode_artifacts_config,
    default="",
    help="Where the checkout artifacts are stored and the build "
    "artifacts are published",
)
@click.option(
    "-w",
    "--workspace",
    type=click.Path(dir_okay=True, file_okay=False),
    default="workspace",
    help="Where to prepare the workspace layout",
)
@click.option(
    "--with-krb5",
    metavar="JSON",
    type=json.loads,
    help="Credentials to obtain a Kerberos token as JSON "
    "object with 'principal' and 'keytab' keys",
)
def test(
    task: TaskId,
    project_platform: str,
    processes: Optional[int],
    artifacts_repo: Mapping,
    workspace: str,
    with_krb5: Optional[Mapping],
):
    """
    Build a specific project for a specific platform (passed as 'project/platform')
    as declared in a slot, publishing the archive with the build artifacts
    to an artifacts repository.
    """
    project_name, platform = project_platform.split("/")

    # FIXME deduplicate
    d = LbNightlyTools.Utils.Dashboard(flavour=task.flavour)
    doc_id = task.doc_id()
    try:
        doc = d[doc_id]
    except couchdb.ResourceNotFound:
        exit(f"unknown build {task}")

    if is_slot_aborted(doc):
        click.echo(f"{task} aborted, no need to test")
        return

    slot = Slot.fromDict(doc["config"])
    if getattr(slot, project_name).no_test:
        click.echo(f"{project_name} {task} has no test")
        return

    update_db(d.db, doc_id, step_info_initializer(["tests", platform, project_name]))

    try:
        workspace_path = Path(workspace).resolve()
        workspace_path.mkdir(parents=True, exist_ok=True)

        repo = connect_repo(artifacts_repo)
        assert repo

        env = common.build_env(platform)
        # we used a special bind for upstream projects (because of ccache)
        # and the test container must match
        # if the directory does not exist it means we do not need it
        # (checked in prepare)
        apptainer_flags = []
        if (DEPLOYMENT_ROOT / str(task)).exists():
            apptainer_flags.append(
                f"--bind '{DEPLOYMENT_ROOT / str(task)}:/dependencies'"
            )

        if "X509_USER_PROXY" in env:
            apptainer_flags.append(
                f"--bind '{env['X509_USER_PROXY']}:{env['X509_USER_PROXY']}'"
            )

        if "APPTAINER_FLAGS" in env:
            apptainer_flags.append(env["APPTAINER_FLAGS"])

        if apptainer_flags:
            env["APPTAINER_FLAGS"] = " ".join(apptainer_flags)

        project: Project = getattr(slot, project_name)

        build_dir = workspace_path / project.baseDir / "build"

        ctest_start_time = datetime.now()
        with (
            contextlib.chdir(workspace_path),
            temp_env(env, clear=True),
            with_krb5_token(**with_krb5) if with_krb5 else contextlib.nullcontext(),
        ):
            # this is the best place where to record the environment used for the build
            build_env = project.environment()
            result = project.test(
                jobs=processes or os.cpu_count(), stderr=subprocess.STDOUT
            )
            # FIXME clarify str or bytes
            if isinstance(result.stdout, str):
                result.stdout = result.stdout.encode("utf-8")

        # build job specific infos for the report
        # mask possible secrets in the environment
        for k in build_env:
            if re.search(r"pass(w(or)?d)?|secret|token|admin", k.lower()):
                build_env[k] = "***"

        if (build_dir / "Testing").is_dir():  # CMake
            # get path to Test.xml
            if (build_dir / "Testing" / "TAG").exists():
                tag = (build_dir / "Testing" / "TAG").read_text().splitlines()[0]
            else:
                # we want to make sure that a tag is always set, even if
                # CTest didn't produce it (e.g. because of a failure)
                tag = "Experimental"
            test_xml_path = build_dir / "Testing" / tag / "Test.xml"
            if not test_xml_path.exists():
                # if there's a failure in CTest, we have to provide
                # a dummy Test.xml to make the dashboard happy
                test_xml_path.parent.mkdir(parents=True, exist_ok=True)
                with test_xml_path.open("w") as f:
                    f.write(
                        CTEST_FAILURE_REPORT_TEMPLATE.format(
                            start_time=int(ctest_start_time.timestamp()),
                            end_time=int(datetime.now().timestamp()),
                            job_url=escape_for_xml(
                                '<a href="{0}">{0}</a>'.format(os.environ["BUILD_URL"])
                                if "BUILD_URL" in os.environ
                                else "<i>unknown</i>"
                            ),
                            stdout=escape_for_xml(result.stdout.decode()),
                        )
                    )

            artifact_path = f"{task}/tests/{platform}/cdash/{project_name}.zip"
            common.publish(
                repo,
                artifact_path,
                build_dir,
                [test_xml_path],
            )

            results = extract_results(test_xml_path)
        else:  # CMT
            html_results = build_dir / "html"

            summary_json = html_results / "summary.json"
            # FIXME I'm not sure this is really needed
            fixFailureCauses(summary_json.as_posix())
            results = extract_results_cmt(summary_json)

            results_dir = workspace_path / project_name / "results"
            if results_dir.exists():
                shutil.rmtree(results_dir)
            if html_results.exists():
                shutil.copytree(html_results, results_dir)

            artifact_path = f"{task}/tests/{platform}/{project_name}.zip"
            common.publish(
                repo,
                artifact_path,
                workspace_path,
                [results_dir],
            )

        # FIXME deduplicate
        cpuinfo = LbNightlyTools.Utils.cpuinfo()

        update_db(
            d.db,
            doc_id,
            lambda doc: doc["tests"][platform][project_name].update(
                {
                    "completed": datetime.now().isoformat(),
                    "retcode": result.returncode,
                    "environment": build_env,
                    # FIXME we may have different CPUs in a machine
                    "cpu": cpuinfo[0] if cpuinfo else {},
                    "results": results,
                }
            ),
        )

    except Exception as e:
        logger.exception(e)
        err = e

        def report_failure(doc):
            report = doc["tests"][platform][project_name]
            report["error"] = str(err)
            report["failed"] = datetime.now().isoformat()

        update_db(
            d.db,
            doc_id,
            report_failure,
        )
        raise


def publish_test_results(
    repo: ArtifactsRepository,
    build_dir: str | Path,
    test_xml_path: Path,
    artifact_path: str,
):
    pack = BytesIO()
    with zipfile.ZipFile(pack, "w", zipfile.ZIP_DEFLATED) as zf:
        zf.write(
            test_xml_path.as_posix(),
            test_xml_path.relative_to(build_dir).as_posix(),
        )

    pack.seek(0)
    if not repo.push(pack, artifact_path):
        raise RuntimeError(f"failed to publish {artifact_path}")


def extract_results(test_xml_path: Path) -> dict[str, list[str]]:
    doc = ET.parse(test_xml_path)
    results = defaultdict(list)
    for test in doc.findall("Testing/Test"):
        status = test.attrib["Status"]
        status = CDASH_STATUS_MAP.get(status, status)

        name_el = test.find("Name")
        if name_el is None:
            continue

        name = name_el.text
        if not name:
            continue

        results[status].append(name)

    for tests in results.values():
        tests.sort()

    return dict(results)


def extract_results_cmt(summary_json: Path) -> dict[str, list[str]]:
    with summary_json.open() as f:
        summaries = json.load(f)

    results = defaultdict(list)
    for result in summaries:
        if "id" in result:
            results[result.get("outcome", "PASS")].append(result["id"])

    for tests in results.values():
        tests.sort()

    return dict(results)


@lb_ci.command()
@click.argument("task", type=TaskId.from_str)
@click.argument("project_platform")
@click.option(
    "-a",
    "--artifacts-repo",
    metavar="ARTIFACTS_REPO_CONFIG",
    type=decode_artifacts_config,
    default="",
    help="Where to store artifacts",
)
@click.option(
    "-w",
    "--workspace",
    type=click.Path(dir_okay=True, file_okay=False),
    default="workspace",
    help="Where the project was copied and tested",
)
def publish_new_refs(
    task: TaskId, project_platform: str, artifacts_repo: Mapping, workspace: str
):
    """
    Collect updated reference files from tests in the source tree (`.new` files),
    pack them in a ZIP file and upload it to the artifacts repository.
    """
    workspace_path = Path(workspace).resolve()
    if not workspace_path.is_dir():
        logger.warning("workspace directory missing, nothing to publish")
        return

    project_name, platform = project_platform.split("/")

    # FIXME deduplicate
    d = LbNightlyTools.Utils.Dashboard(flavour=task.flavour)
    doc_id = task.doc_id()
    try:
        doc = d[doc_id]
    except couchdb.ResourceNotFound:
        exit(f"unknown build {task}")

    slot = Slot.fromDict(doc["config"])
    project: Project = getattr(slot, project_name)

    # FIXME deduplicate
    artifact_path = f"{task}/tests/{platform}/newrefs/{project_name}.zip"
    project_root = workspace_path / project.baseDir
    new_refs = list(project_root.glob("**/*.new"))
    if new_refs:
        repo = connect_repo(artifacts_repo)
        common.publish(
            repo,
            artifact_path,
            project_root,
            new_refs,
        )
    else:
        click.echo("no updated test reference files found: nothing to do")
