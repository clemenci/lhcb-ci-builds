import asyncio
import contextlib
import logging
import os
import re
import sys
import zipfile
from ast import literal_eval
from datetime import datetime
from io import BytesIO
from pathlib import Path
from platform import machine
from socket import gethostname
from subprocess import check_output, run
from tempfile import NamedTemporaryFile
from time import sleep
from typing import Iterable, Mapping, Optional

import couchdb
from LbNightlyTools.Configuration import Project, Slot

from .repository import ArtifactsRepository


class TaskId:
    """
    Helper to manipulate a slot id.
    """

    __slots__ = ("flavour", "slot", "build_id")

    def __init__(self, flavour: str, slot: str, build_id: int) -> None:
        self.flavour = flavour
        self.slot = slot
        self.build_id = build_id

    def __str__(self) -> str:
        return f"{self.flavour}/{self.slot}/{self.build_id}"

    def __repr__(self) -> str:
        return f"TaskId({self.flavour!r}, {self.slot!r}, {self.build_id!r})"

    def __eq__(self, value, /) -> bool:
        return self is value or (
            type(value) is TaskId
            and (
                (self.flavour, self.slot, self.build_id)
                == (value.flavour, value.slot, value.build_id)
            )
        )

    def doc_id(self) -> str:
        """
        Id of the document in CouchDB for this slot build.
        """
        return f"{self.slot}.{self.build_id}"

    @staticmethod
    def from_str(task: str):
        """
        Parse the string representation of a TaskId to a TaskId instance.
        """
        parsed_task = re.match(
            r"(?P<flavour>[^/]+)/(?P<slot>[^/@]+)/(?P<build_id>[0-9]+)$",
            task,
        )
        if not parsed_task:
            raise ValueError(f"'{task}' is not a valid task id")
        return TaskId(
            parsed_task.group("flavour"),
            parsed_task.group("slot"),
            int(parsed_task.group("build_id"), 10),
        )


def lbenv(base_env: Optional[Mapping[str, str]] = None) -> dict[str, str]:
    """
    Return a dict of LbEnv defined environment variables.
    """
    env = dict(base_env if base_env is not None else os.environ)
    # FIXME should be ignore or not the LbEnv environment we are in?
    env.pop("LBENV_SOURCED", None)  # ignore an existing LbEnv environment
    env.update(
        literal_eval(
            check_output(
                [sys.executable, "-m", "LbEnv", "--quiet", "--py"], env=env
            ).decode()
        )
    )
    return env


@contextlib.contextmanager
def temp_env(env: dict[str, str], clear: bool = False):
    old = os.environ.copy()
    if clear:
        os.environ.clear()
    os.environ.update(env)
    try:
        yield
    finally:
        os.environ.clear()
        os.environ.update(old)


def update_db(db, key, changes):
    """
    Update a document in the database.

    @param db: The database to update.
    @param key: The key of the document to update.
    @param changes: a function that updates the document in place.
    """
    retry = 30
    while retry:
        try:
            doc = db.get(key) or {"_id": key}
            changes(doc)
            db.save(doc)
            return doc
        except couchdb.ResourceConflict:
            retry -= 1
            sleep(1)
    raise RuntimeError(f"Failed to update document {key}")


def is_slot_aborted(doc) -> bool:
    """
    Return true if the slot has been flagged as aborted.
    """
    return bool(doc.get("aborted"))


@contextlib.contextmanager
def with_krb5_token(principal: str, keytab: str):
    """
    Provide a valid Kerberos token for the duration of the context,
    as long as it is less than the default token lifetime.
    """
    with NamedTemporaryFile(mode="w") as krb5_cache:
        with temp_env(
            {
                "KRB5CCNAME": f"FILE:{krb5_cache.name}",
            }
        ):
            check_output(
                [
                    "kinit",
                    "-k",
                    "-t",
                    keytab,
                    principal,
                ]
            )
            try:
                yield
            finally:
                check_output(["kdestroy"])


def step_info_initializer(path: list[str], timestamp_key: str = "started", **kwargs):
    path = path.copy()
    kwargs = kwargs.copy()

    def init(doc):
        for key in path:
            doc = doc.setdefault(key, {})
        previous = dict((key, doc.pop(key)) for key in list(doc))
        doc.update(
            {
                "started": datetime.now().isoformat(),
                "previous": previous,
                "host": gethostname(),
                "build_url": os.environ.get("BUILD_URL", ""),
            }
        )
        doc.update(kwargs)

    return init


def clear_locale_env(env: dict[str, str]):
    """
    Modify the environment dictionary removing all locale related
    environment variables.
    """
    for key in list(env):
        if key.startswith("LC_") or key in ("LANG", "LANGUAGE"):
            del env[key]


# list of extensions taken from
# https://en.wikipedia.org/wiki/Tar_(computing)#Suffixes_for_compressed_files
TAR_EXTENSIONS_RE = re.compile(
    r"\.("
    r"tar(\.(bz2|gz|lz|lzma|lzo|xz|Z|zst))?"
    r"|t(b2|bz|bz2|z2|az|gz|lz|xz|Z|aZ|zst)"
    r")$"
)


def _tar_compression_flag(extension: str) -> Optional[str]:
    # list of extensions taken from
    # https://en.wikipedia.org/wiki/Tar_(computing)#Suffixes_for_compressed_files
    if extension == ".tar":
        return None
    # expand short extensions to long ones
    extension = {
        ".tb2": ".tar.bz2",
        ".tbz": ".tar.bz2",
        ".tbz2": ".tar.bz2",
        ".tz2": ".tar.bz2",
        ".tgz": ".tar.gz",
        ".taz": ".tar.gz",
        ".tlz": ".tar.lzma",
        ".txz": ".tar.xz",
        ".tZ": ".tar.Z",
        ".taZ": ".tar.Z",
        ".tzst": ".tar.zst",
    }.get(extension, extension)
    return (
        "--"
        + {
            ".bz2": "bzip2",
            ".gz": "gzip",
            ".lz": "lzip",
            ".lzma": "lzma",
            ".lzo": "lzop",
            ".xz": "xz",
            ".Z": "compress",
            ".zst": "zstd",
        }[extension[4:]]
    )


def publish(
    repo: ArtifactsRepository,
    key: str,
    root_dir: str | Path,
    paths: Iterable[Path],
):
    if key.endswith(".zip"):
        pack = _pack_zip(root_dir, paths)
    elif tar_extension := TAR_EXTENSIONS_RE.search(key):
        pack = _pack_tar(root_dir, paths, tar_extension.group(0))
    else:
        raise RuntimeError(f"unknown archive type for {key}")
    if not repo.push(pack, key):
        raise RuntimeError(f"failed to publish {key}")


def _pack_zip(
    root_dir: str | Path,
    paths: Iterable[Path],
):
    pack = BytesIO()

    def all_files(paths):
        """
        Gather all files from paths, recursing into directories
        """
        for path in paths:
            if path.is_dir():
                yield from all_files(path.iterdir())
            else:
                yield path

    with zipfile.ZipFile(pack, "w", zipfile.ZIP_DEFLATED) as zf:
        for path in all_files(root_dir / path for path in paths):
            zf.write(
                path.as_posix(),
                path.relative_to(root_dir).as_posix(),
            )

    pack.seek(0)
    return pack


def _pack_tar(
    root_dir: str | Path,
    paths: Iterable[Path],
    extension: str,
):
    pack = BytesIO()
    cmd = [
        "tar",
        "--create",
        "--file=-",
    ]
    if compression_flag := _tar_compression_flag(extension):
        cmd.append(compression_flag)
    cmd.extend(
        # ensure we have a relative path
        (root_dir / path).relative_to(root_dir).as_posix()
        for path in paths
    )
    run(
        cmd,
        check=True,
        cwd=root_dir,
        stdout=pack,
    )
    pack.seek(0)
    return pack


def build_env(
    platform: str, base_env: Optional[Mapping[str, str]] = None
) -> dict[str, str]:
    """
    Return a dictionary with the environment needed to build and test
    projects in a reliable and stable way.

    The produced environment takes into account that the build or test
    tasks are run inside a container that keeps the project to build
    in /workspace/data/workspace/Project and the upstream projects
    are in /dependencies
    """
    env = lbenv(base_env)

    # we have to tell CMake where to find the dependencies
    env["CMAKE_PREFIX_PATH"] = f"/dependencies:{env.get('CMAKE_PREFIX_PATH', '')}"
    env["LBENV_CURRENT_WORKSPACE"] = "/dependencies"
    # some target platforms need special environments hacks
    platform_os = platform.split("-")[1]
    if "MYSITEROOT" in env:
        env["PATH"] = f"{env['MYSITEROOT']}/bin/{machine()}-{platform_os}:" + env.get(
            "PATH", ""
        )
    env["PATH"] = f"/workspace/.overrides/{platform_os}/bin:{env.get('PATH', '')}"

    clear_locale_env(env)

    # remove DISPLAY not to confuse ROOT
    env.pop("DISPLAY", None)
    # modern TERM values may cause UTF-8 BOM to be emitted on old OSes
    env.pop("TERM", None)

    # FIXME the build function requires BINARY_TAG to be set
    env["BINARY_TAG"] = platform
    # we set CMTCONFIG to have consistent behaviour with lb-set-platform
    env["CMTCONFIG"] = platform

    return env


DEPLOYMENT_ROOT = Path("/cvmfs/lhcbdev.cern.ch/nightlies")
DEPLOYMENT_CHECK_TIMEOUT = 10 * 60  # 10 minutes
DEPLOYMENT_CHECK_SLEEP = 10  # seconds


async def is_deployed(task: TaskId, project_dir: str, platform: Optional[str]):
    project_root = DEPLOYMENT_ROOT / str(task) / project_dir
    deployment_dir = (
        (project_root / "InstallArea" / platform) if platform else project_root
    )
    try:
        async with asyncio.timeout(DEPLOYMENT_CHECK_TIMEOUT):
            while True:
                # if the directory exists and is not empty, it is deployed
                if deployment_dir.is_dir() and list(deployment_dir.iterdir()):
                    logging.getLogger("is_deployed").debug(
                        f"found {project_dir} deployment at {deployment_dir}"
                    )
                    return True
                await asyncio.sleep(DEPLOYMENT_CHECK_SLEEP)
    except TimeoutError:
        return False


async def check_deployed_deps(
    task: TaskId, slot: Slot, project: Project, platform: str
) -> list[tuple[str, bool]]:
    """
    Check that the dependencies of the project are deployed on CVMFS.

    If some are missing, wait DEPLOYMENT_CHECK_TIMEOUT seconds before giving up.

    Return the list pairs (str, bool) for each dependency and if they
    were found or not.
    """

    async def deployment_status(dep: str) -> tuple[str, bool]:
        p = getattr(slot, dep)
        return (
            dep,
            await is_deployed(
                task,
                p.baseDir,
                platform
                if not (p.platform_independent or str(p.build_tool) == "no-build")
                else None,
            ),
        )

    return await asyncio.gather(
        *[
            deployment_status(dep)
            for dep in project.dependencies()
            if hasattr(slot, dep) and getattr(slot, dep).enabled
        ]
    )
