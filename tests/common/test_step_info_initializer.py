#!/usr/bin/env python3
from unittest.mock import MagicMock

import pytest

from lb.jenkins.common import step_info_initializer


@pytest.fixture
def mock_datetime(monkeypatch):
    mock = MagicMock()
    mock.now.return_value.isoformat.return_value = "2023-06-01T12:00:00"
    monkeypatch.setattr("lb.jenkins.common.datetime", mock)
    return mock


@pytest.fixture
def mock_gethostname(monkeypatch):
    mock = MagicMock(return_value="test-host")
    monkeypatch.setattr("lb.jenkins.common.gethostname", mock)
    return mock


def test_step_info_initializer_basic(mock_datetime, mock_gethostname):
    init_func = step_info_initializer(["step1", "step2"])
    doc = {}
    init_func(doc)

    expected = {
        "step1": {
            "step2": {
                "started": "2023-06-01T12:00:00",
                "previous": {},
                "host": "test-host",
                "build_url": "",
            }
        }
    }
    assert doc == expected


def test_step_info_initializer_with_existing_data():
    init_func = step_info_initializer(["step1"])
    doc = {"step1": {"existing_key": "existing_value"}}

    init_func(doc)

    assert "previous" in doc["step1"]
    assert doc["step1"]["previous"] == {"existing_key": "existing_value"}
    assert "existing_key" not in doc["step1"]


@pytest.mark.parametrize("build_url", ["http://test-url", ""])
def test_step_info_initializer_with_build_url(build_url, monkeypatch):
    monkeypatch.setenv("BUILD_URL", build_url)
    init_func = step_info_initializer(["step1"])
    doc = {}

    init_func(doc)

    assert doc["step1"]["build_url"] == build_url


def test_step_info_initializer_with_kwargs():
    init_func = step_info_initializer(["step1"], extra_key="extra_value")
    doc = {}

    init_func(doc)

    assert doc["step1"]["extra_key"] == "extra_value"


def test_step_info_initializer_multiple_calls():
    init_func = step_info_initializer(["step1"])
    doc1 = {}
    doc2 = {}

    init_func(doc1)
    init_func(doc2)

    assert doc1["step1"]["started"] != doc2["step1"]["started"]
