#!/usr/bin/env python3
import pytest

from lb.jenkins.common import TaskId


def test_parse_task_id():
    assert TaskId.from_str("testing/lhcb-head/1234") == TaskId(
        "testing", "lhcb-head", 1234
    )

    # roundtrip test (redundant)
    t = TaskId("some-flavour", "a-name", 12345)
    assert TaskId.from_str(str(t)) == t

    with pytest.raises(ValueError):
        TaskId.from_str("testing/lhcb-head")
    with pytest.raises(ValueError):
        TaskId.from_str("testing/lhcb-head/dummy")


def test_task_id():
    task = TaskId("nightly", "super-test", 765)
    assert str(task) == "nightly/super-test/765"
    assert task.doc_id() == "super-test.765"
    assert repr(task) == "TaskId('nightly', 'super-test', 765)"
