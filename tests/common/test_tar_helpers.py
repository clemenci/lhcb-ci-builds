import pytest

from lb.jenkins.common import TAR_EXTENSIONS_RE, _tar_compression_flag


def test_tar_extension():
    assert _tar_compression_flag(".tar") is None


@pytest.mark.parametrize(
    "extension, expected",
    [
        (".tar.bz2", "--bzip2"),
        (".tar.gz", "--gzip"),
        (".tar.xz", "--xz"),
        (".tar.zst", "--zstd"),
    ],
)
def test_long_extensions(extension, expected):
    assert _tar_compression_flag(extension) == expected


@pytest.mark.parametrize(
    "extension, expected",
    [
        (".tb2", "--bzip2"),
        (".tbz", "--bzip2"),
        (".tgz", "--gzip"),
        (".txz", "--xz"),
        (".tzst", "--zstd"),
    ],
)
def test_short_extensions(extension, expected):
    assert _tar_compression_flag(extension) == expected


@pytest.mark.parametrize(
    "extension, expected",
    [
        (".tar.lz", "--lzip"),
        (".tar.lzma", "--lzma"),
        (".tar.lzo", "--lzop"),
        (".tar.Z", "--compress"),
    ],
)
def test_other_extensions(extension, expected):
    assert _tar_compression_flag(extension) == expected


def test_invalid_extension():
    with pytest.raises(KeyError):
        _tar_compression_flag(".invalid")


@pytest.mark.parametrize(
    "extension",
    [
        ".tar",
        ".tar.bz2",
        ".tar.gz",
        ".tar.lz",
        ".tar.lzma",
        ".tar.lzo",
        ".tar.xz",
        ".tar.Z",
        ".tar.zst",
        ".tb2",
        ".tbz",
        ".tbz2",
        ".tz2",
        ".taz",
        ".tgz",
        ".tlz",
        ".txz",
        ".tZ",
        ".taZ",
        ".tzst",
    ],
)
def test_tar_extensions_match(extension):
    assert TAR_EXTENSIONS_RE.search(f"file{extension}") is not None


@pytest.mark.parametrize(
    "extension",
    [
        ".zip",
        ".rar",
        ".7z",
        ".txt",
        ".pdf",
        ".doc",
        ".tar.invalid",
        ".tarbz2",
        ".tar.bz3",
        ".tar.gzip",
    ],
)
def test_tar_extensions_no_match(extension):
    assert TAR_EXTENSIONS_RE.search(f"file{extension}") is None


def test_case_sensitivity():
    assert TAR_EXTENSIONS_RE.search("file.TAR") is None


def test_match_at_end_only():
    assert TAR_EXTENSIONS_RE.search("file.tar.gz") is not None
    assert TAR_EXTENSIONS_RE.search("file.tar.gz.extra") is None


@pytest.mark.parametrize("filename", ["long_file_name.tar.bz2", "path/to/file.tgz"])
def test_match_with_preceding_text(filename):
    assert TAR_EXTENSIONS_RE.search(filename) is not None
