from typing import Any

import couchdb
import LbNightlyTools.Utils


class MockDashboardFactory:
    def __init__(self, docs: dict[str, Any]):
        self.docs = docs
        self.dashboard = None

    def __call__(self, flavour=None):
        class Dashboard:
            def __init__(self, flavour=None, docs=None):
                class MockDB:
                    def __init__(self, docs):
                        self.docs = docs

                    def save(self, doc):
                        self.docs[doc["_id"]] = doc

                    def get(self, doc, default=None):
                        return self.docs.get(doc, default)

                self.db = MockDB(docs or {})
                self.flavour = flavour

            def __getitem__(self, key):
                if key not in self.db.docs:
                    raise couchdb.ResourceNotFound(f"{key} is missing")
                doc = self.db.docs[key]
                doc["_id"] = key
                return doc

            def lastBuildId(self, slot):
                ids = [
                    doc.get("build_id", 0)
                    for doc in self.db.docs.values()
                    if doc.get("slot") == slot
                ]
                if ids:
                    return max(ids)
                return 0

        self.dashboard = Dashboard(flavour, self.docs)
        return self.dashboard

    def set_docs(self, docs):
        self.docs = docs


def mock_dashboard(
    monkeypatch, docs: dict[str, Any], module=None
) -> MockDashboardFactory:
    mock = MockDashboardFactory(docs)
    monkeypatch.setattr(module or LbNightlyTools.Utils, "Dashboard", mock)
    return mock
