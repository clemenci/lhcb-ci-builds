import asyncio

import pytest

import lb.jenkins.build as build
from lb.jenkins.common import TaskId

TASK = TaskId.from_str("testing/some-slot/1234")
PROJECT = "MyProject"
PLATFORM = "x86_64_v2-el9-gcc13-opt"


@pytest.mark.asyncio
async def test_already_deployed(monkeypatch, tmp_path):
    monkeypatch.setattr(build, "DEPLOYMENT_ROOT", tmp_path)

    target_dir = tmp_path / str(TASK) / PROJECT / "InstallArea" / PLATFORM
    target_dir.mkdir(parents=True)
    target_dir.joinpath(".deployed").touch()

    assert await build.is_deployed(TASK, PROJECT, PLATFORM)


@pytest.mark.asyncio
async def test_retry_check(monkeypatch, tmp_path):
    monkeypatch.setattr(build, "DEPLOYMENT_ROOT", tmp_path)
    monkeypatch.setattr(build, "DEPLOYMENT_CHECK_SLEEP", 0.01)

    target_dir = tmp_path / str(TASK) / PROJECT / "InstallArea" / PLATFORM
    target_dir.mkdir(parents=True)

    # start checking
    is_deployed = build.is_deployed(TASK, PROJECT, PLATFORM)

    await asyncio.sleep(0.2)

    # mimic deployment
    target_dir.joinpath(".deployed").touch()

    assert await is_deployed


@pytest.mark.asyncio
async def test_failure(monkeypatch, tmp_path):
    monkeypatch.setattr(build, "DEPLOYMENT_ROOT", tmp_path)
    monkeypatch.setattr(build, "DEPLOYMENT_CHECK_TIMEOUT", 0.1)

    target_dir = tmp_path / str(TASK) / PROJECT / "InstallArea" / PLATFORM
    target_dir.mkdir(parents=True)

    assert not await build.is_deployed(TASK, PROJECT, PLATFORM)


@pytest.mark.asyncio
async def test_data_project(monkeypatch, tmp_path):
    monkeypatch.setattr(build, "DEPLOYMENT_ROOT", tmp_path)

    target_dir = tmp_path / str(TASK) / PROJECT
    target_dir.mkdir(parents=True)
    target_dir.joinpath(".deployed").touch()

    assert await build.is_deployed(TASK, PROJECT, None)


@pytest.mark.asyncio
async def test_data_project_failure(monkeypatch, tmp_path):
    monkeypatch.setattr(build, "DEPLOYMENT_ROOT", tmp_path)
    monkeypatch.setattr(build, "DEPLOYMENT_CHECK_TIMEOUT", 0.1)

    target_dir = tmp_path / str(TASK) / PROJECT / "InstallArea" / PLATFORM
    target_dir.mkdir(parents=True)

    assert not await build.is_deployed(TASK, PROJECT, None)
