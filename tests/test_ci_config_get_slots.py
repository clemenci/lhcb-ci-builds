import json
import os

from click.testing import CliRunner
from LbNightlyTools.Configuration import Project, Slot

import lb.jenkins.config
from lb.jenkins.ci import lb_ci


def test_default_repo(tmp_path, monkeypatch):
    global requested_repo
    requested_repo = None

    def get_dummy_slot(repository):
        global requested_repo
        requested_repo = repository
        return [Slot("dummy", projects=[Project("SomeProject", "v1r20")])]

    monkeypatch.setattr(lb.jenkins.config, "get_slot_configs", get_dummy_slot)

    runner = CliRunner()
    with runner.isolated_filesystem(temp_dir=tmp_path) as td:
        result = runner.invoke(lb_ci, ["config", "get-slots", "-o", "slots.json"])
        assert result.exit_code == 0
        assert requested_repo == lb.jenkins.config.DEFAULT_CONF_REPO
        slots = json.load(open(os.path.join(td, "slots.json")))
        assert len(slots) == 1
        slot = Slot.fromDict(slots[0])
        assert slot.name == "dummy"
        assert hasattr(slot, "SomeProject")
        assert slot.SomeProject.version == "v1r20"


def test_custom_version(tmp_path, monkeypatch):
    global requested_repo
    requested_repo = None

    def get_dummy_slot(repository):
        global requested_repo
        requested_repo = repository
        return []

    monkeypatch.setattr(lb.jenkins.config, "get_slot_configs", get_dummy_slot)

    runner = CliRunner()
    with runner.isolated_filesystem(temp_dir=tmp_path):
        result = runner.invoke(
            lb_ci,
            [
                "config",
                "get-slots",
                "-o",
                "slots.json",
                "--config-repo",
                "#some-version",
            ],
        )
        assert result.exit_code == 0
        assert requested_repo == lb.jenkins.config.DEFAULT_CONF_REPO + "#some-version"


def test_no_slots(tmp_path, monkeypatch):
    def get_dummy_slot(_repository):
        return []

    monkeypatch.setattr(lb.jenkins.config, "get_slot_configs", get_dummy_slot)

    runner = CliRunner()
    with runner.isolated_filesystem(temp_dir=tmp_path) as td:
        result = runner.invoke(lb_ci, ["config", "get-slots", "-o", "slots.json"])
        assert result.exit_code == 0
        slots = json.load(open(os.path.join(td, "slots.json")))
        assert len(slots) == 0


def test_just_enabled(tmp_path, monkeypatch):
    def get_dummy_slot(_repository):
        return [
            Slot("dummy", projects=[Project("SomeProject", "v1r20")]),
            Slot("ignored", projects=[Project("OtherProject", "v0r0")], disabled=True),
        ]

    monkeypatch.setattr(lb.jenkins.config, "get_slot_configs", get_dummy_slot)

    runner = CliRunner()
    with runner.isolated_filesystem(temp_dir=tmp_path) as td:
        result = runner.invoke(lb_ci, ["config", "get-slots", "-o", "slots.json"])
        assert result.exit_code == 0
        slots = json.load(open(os.path.join(td, "slots.json")))
        assert len(slots) == 1
        assert slots[0]["slot"] == "dummy"


def test_include_disabled(tmp_path, monkeypatch):
    def get_dummy_slot(_repository):
        return [
            Slot("dummy", projects=[Project("SomeProject", "v1r20")]),
            Slot("ignored", projects=[Project("OtherProject", "v0r0")], disabled=True),
        ]

    monkeypatch.setattr(lb.jenkins.config, "get_slot_configs", get_dummy_slot)

    runner = CliRunner()
    with runner.isolated_filesystem(temp_dir=tmp_path) as td:
        result = runner.invoke(
            lb_ci, ["config", "get-slots", "-o", "slots.json", "--all-slots"]
        )
        assert result.exit_code == 0
        slots = json.load(open(os.path.join(td, "slots.json")))
        assert len(slots) == 2
        assert slots[0]["slot"] == "dummy"
        assert slots[1]["slot"] == "ignored"


def test_specific_slot(tmp_path, monkeypatch):
    def get_dummy_slot(_repository):
        return [
            Slot("dummy", projects=[Project("SomeProject", "v1r20")]),
            Slot("ignored", projects=[Project("OtherProject", "v0r0")], disabled=True),
        ]

    monkeypatch.setattr(lb.jenkins.config, "get_slot_configs", get_dummy_slot)

    runner = CliRunner()
    with runner.isolated_filesystem(temp_dir=tmp_path) as td:
        result = runner.invoke(
            lb_ci, ["config", "get-slots", "-o", "slots.json", "ignored"]
        )
        assert result.exit_code == 0
        slots = json.load(open(os.path.join(td, "slots.json")))
        assert len(slots) == 1
        assert slots[0]["slot"] == "ignored"


def test_bad_slot(tmp_path, monkeypatch):
    global requested_repo
    requested_repo = None

    def get_dummy_slot(repository):
        global requested_repo
        requested_repo = repository
        return [
            Slot("dummy", projects=[Project("SomeProject", "v1r20")]),
            Slot("ignored", projects=[Project("OtherProject", "v0r0")], disabled=True),
        ]

    monkeypatch.setattr(lb.jenkins.config, "get_slot_configs", get_dummy_slot)

    runner = CliRunner()
    with runner.isolated_filesystem(temp_dir=tmp_path):
        result = runner.invoke(
            lb_ci, ["config", "get-slots", "-o", "slots.json", "bad"]
        )
        assert result.exit_code == 2
