from copy import deepcopy
from datetime import date

import pytest
from conftest import mock_dashboard

import lb.jenkins.config
from lb.jenkins.config import ToBuild


@pytest.fixture
def dashboard(monkeypatch):
    return mock_dashboard(
        monkeypatch,
        {},
        module=lb.jenkins.config,
    )


@pytest.fixture
def to_build(dashboard):
    return ToBuild(flavour="test_flavour", ref_date=date(2024, 1, 1), max_age=7)


def test_init():
    tb = ToBuild("test_flavour", date(2024, 1, 1), 7)
    assert tb.flavour == "test_flavour"
    assert tb.ref_date == date(2024, 1, 1)
    assert tb.max_age == 7
    assert tb._dashboard is None


def test_dashboard_property(dashboard):
    tb = ToBuild("test_flavour", date(2024, 1, 1), 7)
    assert tb.dashboard.flavour == "test_flavour"
    assert tb._dashboard is dashboard.dashboard  # Check if it's the right instance


def test_clean_slot_dict():
    tb = ToBuild("test_flavour", date(2024, 1, 1), 7)

    test_dict = {
        "build_id": "123",
        "projects": [
            {"name": "proj2", "dependencies": ["dep1"]},
            {"name": "proj1", "dependencies": ["dep2"]},
        ],
        "packages": [{"name": "pkg2"}, {"name": "pkg1"}],
        "metadata": {
            "config_id": "conf1",
            "flavour": "test",
            "ci_test": {"trigger": "manual"},
        },
    }

    expected_dict = {
        "projects": [{"name": "proj1"}, {"name": "proj2"}],
        "packages": [{"name": "pkg1"}, {"name": "pkg2"}],
        "metadata": {"ci_test": {}},
    }

    tb.clean_slot_dict(test_dict)
    assert test_dict == expected_dict


def test_call_aborted_build(dashboard, to_build):
    dashboard.set_docs(
        {"test_slot.123": {"slot": "test_slot", "build_id": 123, "aborted": True}}
    )

    result = to_build({"slot": "test_slot"})
    assert result is True


def test_call_old_build(dashboard, to_build):
    dashboard.set_docs(
        {"test_slot.123": {"slot": "test_slot", "build_id": 123, "date": "2023-01-01"}}
    )

    result = to_build({"slot": "test_slot"})
    assert result is True


def test_call_unchanged_config(dashboard, to_build):
    test_config = {
        "slot": "test_slot",
        "projects": [{"name": "proj1"}],
        "packages": [{"name": "pkg1"}],
        "metadata": {},
    }

    dashboard.set_docs(
        {
            "test_slot.123": {
                "slot": "test_slot",
                "build_id": 123,
                "date": "2023-12-31",
                "config": deepcopy(test_config),
            }
        }
    )

    result = to_build(test_config)
    assert result is False


def test_call_changed_config(dashboard, to_build):
    old_config = {
        "slot": "test_slot",
        "projects": [{"name": "proj1"}],
        "packages": [{"name": "pkg1"}],
        "metadata": {},
    }

    new_config = {
        "slot": "test_slot",
        "projects": [{"name": "proj1"}, {"name": "proj2"}],
        "packages": [{"name": "pkg1"}],
        "metadata": {},
    }

    dashboard.set_docs(
        {
            "test_slot.123": {
                "slot": "test_slot",
                "build_id": 123,
                "date": "2023-12-31",
                "config": deepcopy(old_config),
            }
        }
    )

    result = to_build(new_config)
    assert result is True
