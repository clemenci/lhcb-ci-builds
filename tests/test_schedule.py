import os
from pathlib import Path

import pytest
from click.testing import CliRunner
from conftest import mock_dashboard
from LbNightlyTools.Configuration import DataProject, Package, Project, Slot

from lb.jenkins.ci import lb_ci
from lb.jenkins.schedule import (
    builds_pending,
    is_checkout_done,
    is_checkout_running,
    is_slot_aborted,
    preconditions_to_check,
)


def test_checkout_check():
    assert is_checkout_done({}) is False
    assert is_checkout_done({"checkout": {}}) is False
    assert is_checkout_done({"checkout": {"completed": ""}}) is False
    assert is_checkout_done({"checkout": {"completed": "some date"}}) is True

    assert is_checkout_running({}) is False
    assert is_checkout_running({"checkout": {"scheduled": "one hour ago"}}) is True
    assert is_checkout_running({"checkout": {"started": "now"}}) is True
    assert is_checkout_running({"checkout": {"completed": "some date"}}) is False


def test_slot_aborted():
    assert is_slot_aborted({}) is False
    assert is_slot_aborted({"aborted": {}}) is False
    assert is_slot_aborted({"aborted": {"user": "me", "time": "now"}})


P1 = "x86_64_v2-el9-gcc13-opt"
P2 = "x86_64_v2-el9-gcc13-dbg"
P3 = "x86_64_v3-el9-gcc13-opt"


@pytest.mark.parametrize(
    "slot,expected,doc_changes",
    [
        (Slot("test", projects=[], platforms=[P1]), [], None),
        (
            Slot(
                "test",
                projects=[DataProject("DBASE", packages=[Package("Dummy", "v1r0")])],
                platforms=[P1],
            ),
            [],
            None,
        ),
        (Slot("test", projects=[DataProject("DBASE")], platforms=[P1]), [], None),
        (
            Slot(
                "test",
                projects=[Project("Gaudi", "head", disabled=True)],
                platforms=[P1],
            ),
            [],
            None,
        ),
        (
            Slot("test", projects=[Project("Gaudi", "head")], platforms=[P1]),
            [("Gaudi", P1)],
            None,
        ),
        (
            Slot("test", projects=[Project("Gaudi", "head")], platforms=[P1, P2]),
            [("Gaudi", P1), ("Gaudi", P2)],
            None,
        ),
        (
            Slot(
                "test",
                projects=[
                    Project("Gaudi", "head"),
                    Project("LHCb", "head", dependencies=["Gaudi"]),
                ],
                platforms=[P1],
            ),
            [("Gaudi", P1)],
            None,
        ),
        (
            Slot(
                "test",
                projects=[
                    Project("Gaudi", "head"),
                    Project("LHCb", "head", dependencies=["Gaudi"]),
                ],
                platforms=[P1, P2],
            ),
            [("LHCb", P2)],
            {
                "builds": {
                    P1: {"Gaudi": {"scheduled": "now"}},
                    P2: {"Gaudi": {"completed": "now"}},
                },
                "deployment": {
                    f"Gaudi/{P2}": {
                        "status": "SUCCESS",
                    }
                },
            },
        ),
        (
            Slot(
                "test",
                projects=[
                    Project("Gaudi", "head"),
                    Project("LHCb", "head", dependencies=["Gaudi"]),
                ],
                platforms=[P1, P2, P3],
                preconditions=[{"name": "check", "args": {}}],
            ),
            [("Gaudi", P1)],
            {
                "preconditions": {
                    P1: {"status": "ok"},
                    P2: {"started": "now"},
                    P3: {"status": "bad"},
                }
            },
        ),
    ],
)
def test_builds_pending(slot, expected, doc_changes):
    expected.sort()
    doc = {"config": slot.toDict()}
    if doc_changes:
        doc.update(doc_changes)
    found = builds_pending(doc)
    found.sort()
    assert found == expected


def test_command_missing_slot(tmp_path, monkeypatch):
    mock_dashboard(monkeypatch, {})
    runner = CliRunner()
    with runner.isolated_filesystem(temp_dir=tmp_path) as td:
        data_dir = Path(td) / "data"
        result = runner.invoke(lb_ci, ["schedule", "testing/some-slot/1234"])
        assert result.exit_code != 0
        assert "testing/some-slot/1234" in result.stdout
        assert not data_dir.exists()


def test_command_checkout_needed(tmp_path, monkeypatch):
    mock = mock_dashboard(monkeypatch, {"some-slot.1234": {}})
    runner = CliRunner()
    with runner.isolated_filesystem(temp_dir=tmp_path) as td:
        data_dir = Path(td) / "data"
        result = runner.invoke(
            lb_ci, ["schedule", "--next-tasks-dir", "data", "testing/some-slot/1234"]
        )
        assert result.exit_code == 0
        assert mock.dashboard and mock.dashboard.flavour == "testing"
        assert data_dir.is_dir()
        assert [p.name for p in data_dir.iterdir()] == ["checkout.txt"]
        expected = "task=testing/some-slot/1234\n"
        assert (data_dir / "checkout.txt").read_text() == expected


def test_command_slot_aborted(tmp_path, monkeypatch):
    mock = mock_dashboard(monkeypatch, {"some-slot.1234": {"aborted": {"user": "me"}}})
    runner = CliRunner()
    with runner.isolated_filesystem(temp_dir=tmp_path) as td:
        data_dir = Path(td) / "data"
        data_dir.mkdir()
        result = runner.invoke(
            lb_ci, ["schedule", "--next-tasks-dir", "data", "testing/some-slot/1234"]
        )
        assert result.exit_code == 0
        assert mock.dashboard and mock.dashboard.flavour == "testing"
        assert not [p.name for p in data_dir.iterdir()]
        assert "aborted" in result.stdout


def test_command_nothing_to_build(tmp_path, monkeypatch):
    mock = mock_dashboard(
        monkeypatch,
        {
            "some-slot.1234": {
                "checkout": {"completed": "today"},
                "config": Slot(
                    "some-slot",
                    build_id=1234,
                    projects=[
                        # Project("Gaudi", "head"),
                        # Project("LHCb", "head", dependencies=["Gaudi"]),
                    ],
                    platforms=[P1, P2],
                ).toDict(),
            }
        },
    )
    runner = CliRunner()
    with runner.isolated_filesystem(temp_dir=tmp_path) as td:
        data_dir = Path(td) / "data"
        data_dir.mkdir()
        result = runner.invoke(
            lb_ci, ["schedule", "--next-tasks-dir", "data", "testing/some-slot/1234"]
        )
        assert result.exit_code == 0
        assert mock.dashboard and mock.dashboard.flavour == "testing"
        assert not [p.name for p in data_dir.iterdir()]
        assert "nothing to do for testing/some-slot/1234" in result.stdout


def test_command_build_chain(tmp_path, monkeypatch):
    mock = mock_dashboard(
        monkeypatch,
        {
            "some-slot.1234": {
                "checkout": {"completed": "today"},
                "config": Slot(
                    "some-slot",
                    build_id=1234,
                    projects=[
                        Project("Gaudi", "head"),
                        Project("LHCb", "head", dependencies=["Gaudi"]),
                    ],
                    platforms=[P1],
                ).toDict(),
            }
        },
    )
    runner = CliRunner()
    with runner.isolated_filesystem(temp_dir=tmp_path) as td:
        data_dir = Path(td) / "data"
        data_dir.mkdir()
        result = runner.invoke(
            lb_ci, ["schedule", "--next-tasks-dir", "data", "testing/some-slot/1234"]
        )
        assert result.exit_code == 0
        assert mock.dashboard and mock.dashboard.flavour == "testing"
        assert data_dir.is_dir()
        # schedule first project
        assert [p.name for p in data_dir.iterdir()] == ["build-0.txt"]
        expected = (
            "task=testing/some-slot/1234\nnode=build && x86_64_v2\n"
            f"project=Gaudi/{P1}\n"
        )
        assert (data_dir / "build-0.txt").read_text() == expected
        assert mock.dashboard.db.docs["some-slot.1234"]["builds"][P1]["Gaudi"][
            "scheduled"
        ]
        # pretend the first is done and deployed
        mock.dashboard.db.docs["some-slot.1234"]["builds"][P1]["Gaudi"]["completed"] = (
            "today"
        )
        mock.dashboard.db.docs["some-slot.1234"]["deployment"] = {
            f"Gaudi/{P1}": {"status": "SUCCESS"}
        }
        # schedule second project
        result = runner.invoke(
            lb_ci, ["schedule", "--next-tasks-dir", "data", "testing/some-slot/1234"]
        )
        assert result.exit_code == 0
        assert [p.name for p in data_dir.iterdir()] == ["build-0.txt"]
        expected = (
            "task=testing/some-slot/1234\nnode=build && x86_64_v2\n"
            f"project=LHCb/{P1}\n"
        )
        assert (data_dir / "build-0.txt").read_text() == expected
        assert mock.dashboard.db.docs["some-slot.1234"]["builds"][P1]["LHCb"][
            "scheduled"
        ]
        # pretend the second is done
        mock.dashboard.db.docs["some-slot.1234"]["builds"][P1]["LHCb"]["completed"] = (
            "today"
        )
        # nothing left to schedule
        os.remove(data_dir / "build-0.txt")
        result = runner.invoke(
            lb_ci, ["schedule", "--next-tasks-dir", "data", "testing/some-slot/1234"]
        )
        assert result.exit_code == 0
        assert not [p.name for p in data_dir.iterdir()]
        assert "nothing to do for testing/some-slot/1234" in result.stdout


@pytest.mark.parametrize(
    "override, platform, expected_node",
    [
        (None, P1, "build && x86_64_v2"),
        ("custom && <default>", P1, "custom && build && x86_64_v2"),
        ("fully_custom_label", P1, "fully_custom_label"),
        ("fully_custom_label", P2, "build && x86_64_v2"),
    ],
)
def test_command_override_node(
    tmp_path, monkeypatch, override, platform, expected_node
):
    mock = mock_dashboard(
        monkeypatch,
        {
            "some-slot.1234": {
                "checkout": {"completed": "today"},
                "config": Slot(
                    "some-slot",
                    build_id=1234,
                    projects=[
                        Project("Gaudi", "head"),
                    ],
                    platforms=[P1],
                    metadata={"node_overrides": {f"Gaudi/{platform}": override}},
                ).toDict(),
            }
        },
    )
    runner = CliRunner()
    with runner.isolated_filesystem(temp_dir=tmp_path) as td:
        data_dir = Path(td) / "data"
        data_dir.mkdir()
        result = runner.invoke(
            lb_ci, ["schedule", "--next-tasks-dir", "data", "testing/some-slot/1234"]
        )
        assert result.exit_code == 0
        assert mock.dashboard and mock.dashboard.flavour == "testing"
        assert data_dir.is_dir()
        # schedule first project
        assert [p.name for p in data_dir.iterdir()] == ["build-0.txt"]
        assert f"node={expected_node}\n" in (data_dir / "build-0.txt").read_text()


@pytest.mark.parametrize(
    "doc, expected",
    [
        (
            {
                "config": {
                    "platforms": ["platform1", "platform2"],
                }
            },
            [],
        ),
        (
            {"config": {"platforms": ["platform1", "platform2"], "preconditions": []}},
            [],
        ),
        (
            {
                "config": {
                    "platforms": ["platform1", "platform2"],
                    "preconditions": [{"name": "precond1", "args": {}}],
                }
            },
            ["platform1", "platform2"],
        ),
        (
            {
                "config": {
                    "platforms": ["platform1", "platform2", "platform3"],
                    "preconditions": [{"name": "precond1", "args": {}}],
                },
                "preconditions": {
                    "platform1": {"status": "ok"},
                    "platform2": {"status": "not-ok"},
                },
            },
            ["platform3"],
        ),
        (
            {
                "config": {
                    "platforms": ["platform1", "platform2"],
                    "preconditions": [{"name": "precond1", "args": {}}],
                },
                "preconditions": {
                    "platform1": {"status": "ok"},
                    "platform2": {"status": "ok"},
                },
            },
            [],
        ),
        (
            {
                "config": {
                    "platforms": [],
                    "preconditions": [{"name": "precond1", "args": {}}],
                }
            },
            [],
        ),
    ],
)
def test_preconditions_to_check(doc, expected):
    assert set(preconditions_to_check(doc)) == set(expected)
