from pathlib import Path
from zipfile import ZipFile

from click.testing import CliRunner
from conftest import mock_dashboard
from LbNightlyTools.Configuration import Project, Slot

from lb.jenkins.ci import lb_ci


def setup_db(monkeypatch):
    return mock_dashboard(
        monkeypatch,
        {
            "some-slot.1234": {
                "config": Slot(
                    "some-slot",
                    build_id=1234,
                    projects=[
                        Project("Gaudi", "head"),
                        Project("LHCb", "head", dependencies=["Gaudi"]),
                    ],
                    platforms=["x86_64_v2-el9-gcc13-opt"],
                ).toDict(),
            }
        },
    )


def setup_workspace(temp_dir: str):
    refs_dir = Path(temp_dir) / "workspace" / "LHCb" / "TestDir" / "tests" / "refs"
    refs_dir.mkdir(parents=True)
    with refs_dir.joinpath("test_ref.txt.new").open("w") as ref:
        ref.write("new data\n")


def test_nothing_to_publish(monkeypatch, tmp_path):
    setup_db(monkeypatch)
    runner = CliRunner()
    with runner.isolated_filesystem(temp_dir=tmp_path) as td:
        setup_workspace(td)
        repo_dir = Path(td) / "repo"
        result = runner.invoke(
            lb_ci,
            [
                "publish-new-refs",
                "--artifacts-repo",
                str(repo_dir),
                "testing/some-slot/1234",
                "Gaudi/x86_64_v2-el9-gcc13-opt",
            ],
        )
        assert result.exit_code == 0
        assert "nothing to do" in result.stdout
        assert not repo_dir.exists()
        assert not list(repo_dir.glob("**/*"))


def test_publishing(monkeypatch, tmp_path):
    setup_db(monkeypatch)
    runner = CliRunner()
    with runner.isolated_filesystem(temp_dir=tmp_path) as td:
        setup_workspace(td)
        repo_dir = Path(td) / "repo"
        result = runner.invoke(
            lb_ci,
            [
                "publish-new-refs",
                "--artifacts-repo",
                str(repo_dir),
                "testing/some-slot/1234",
                "LHCb/x86_64_v2-el9-gcc13-opt",
            ],
        )
        assert result.exit_code == 0
        artifact = (
            repo_dir
            / "testing/some-slot/1234/tests/x86_64_v2-el9-gcc13-opt/newrefs/LHCb.zip"
        )
        assert artifact.exists()
        newref = "TestDir/tests/refs/test_ref.txt.new"
        with ZipFile(artifact) as zf:
            assert newref in zf.namelist()
            assert zf.read(newref) == b"new data\n"
